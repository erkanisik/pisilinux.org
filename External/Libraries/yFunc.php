<?php 
  /*
  * Created By: Erkan IŞIK
  * Created Date: 2017-09-05
  * Update Date : 2018-01-07
  */

class YFunc {
  
	public static function  KategoriListesi($id = 0,$secim = 0,$tire = 0,$select = ''){
    $sorgu = DB::where('kat_ustid',$id)->get('kategoriler')->result();
    $se = '';

   foreach ($sorgu as $key) {
    if ($key->kat_ustid == 0) { 
      $tire = 0;
      $style = 'color:#000;font-weight:bolder;font-size:13px;';
    }

    if ($secim != $key->kat_ustid){
      $secim = $key->kat_ustid;
      $style = 'color:#000';
      $tire++;
    }

    if($key->id == $select){$sel = 'selected';}else{$sel='';}

    echo '<option value="'.$key->id.'" style="'.$style.'"'.$se.$sel.' >'.str_repeat('-&nbsp;',$tire).$key->adi."</option>";
    yfunc::KategoriListesi($key->id,$secim,$tire,$select);       
  }
}

public static function  KategoriTablosu($id = 0,$secim = 0,$tire = 0){
  $sorgu = DB::where('kat_ustid',$id)->get('kategoriler')->result();

  foreach ($sorgu as $key) {

    if ($key->kat_ustid == 0) {
      $tire = 0;
      $style = 'color:#000;font-weight:bolder;font-size:13px;';
    }

    if ($secim != $key->kat_ustid){
      $secim = $key->kat_ustid;
      $style = 'color:#000';
      $tire++;
    }

    $duzenle = baseUrl('panel/category/edit/'.$key->id);
    $sil = baseUrl('panel/category/delete/'.$key->id);

    echo '<tr>
    <td style="'.$style.'">'.str_repeat('-&nbsp;',$tire).$key->adi.'</td>
    <td>'.$key->aciklama.'</td>
    <td class="islemler" style="text-align: center;"> 
     <a href="'.$duzenle.'"><i class="fa fa-pencil"></i></a>
     <a href="'.$sil.'" onclick="return confirm(\'Bu kaydı silmek istediğinize eminmisiniz?\')"><i class="fa fa-remove"></i></a> 
   </td>
 </tr>';
 yFunc::KategoriTablosu($key->id,$secim,$tire);
}
}
/* Start Wiki  kategorilerini tablo düzeninde listeleme fonksiyonu */
public static function  WikiKategoriTablosu($id = 0,$secim = 0,$tire = 0){
  $sorgu = DB::where('kat_ustid',$id)->get('wiki_kat')->result();

  foreach ($sorgu as $key) {

    if ($key->kat_ustid == 0) {
      $tire = 0;
      $style = 'color:#000;font-weight:bolder;font-size:13px;';
    }

    if ($secim != $key->kat_ustid){
      $secim = $key->kat_ustid;
      $style = 'color:#000';
      $tire++;
    }

    $duzenle = baseUrl('panel/category/edit/'.$key->id);
    $sil = baseUrl('panel/category/delete/'.$key->id);

    echo '<tr>
    <td style="'.$style.'">'.str_repeat('-&nbsp;',$tire).$key->adi.'</td>
    <td class="islemler" style="text-align: center;"> 
     <a href="'.$duzenle.'"><i class="fa fa-pencil"></i></a>
     <a href="'.$sil.'" onclick="return confirm(\'Bu kaydı silmek istediğinize eminmisiniz?\')"><i class="fa fa-remove"></i></a> 
   </td>
 </tr>';
 yFunc::WikiKategoriTablosu($key->id,$secim,$tire);
}
}
/* Stop Wiki  kategorilerini tablo düzeninde listeleme fonksiyonu */

/* Start forum kategorilerini tablo düzeninde listeleme fonksiyonu */
public static function  forumKategoriTablosu($id = 0,$secim = 0,$tire = 0){
  $sorgu = DB::where('kat_ustid',$id)->get('forum_kat')->result();

  foreach ($sorgu as $key) {

    if ($key->kat_ustid == 0) {
      $tire = 0;
      $style = 'color:#000;font-weight:bolder;font-size:13px;';
    }

    if ($secim != $key->kat_ustid){
      $secim = $key->kat_ustid;
      $style = 'color:#000';
      $tire++;
    }

    $duzenle = baseUrl('panel/forum/edit/'.$key->id);
    $sil = baseUrl('panel/forum/delete/'.$key->id);

    echo '<tr>
    <td style="'.$style.'">'.str_repeat('-&nbsp;',$tire).$key->adi.'</td>
    <td>'.$key->aciklama.'</td>
    <td class="islemler" style="text-align: center;"> 
     <a href="'.$duzenle.'"><i class="fa fa-pencil"></i></a>
     <a href="'.$sil.'" onclick="return confirm(\'Bu kaydı silmek istediğinize eminmisiniz?\')"><i class="fa fa-remove"></i></a> 
   </td>
 </tr>';
 yFunc::forumKategoriTablosu($key->id,$secim,$tire);
}
}
/* Stop forum kategorilerini tablo düzeninde listeleme fonksiyonu */

public static function ykat($id = ''){

  if ($id) {
    return DB::where('kat_ustid',$id)->get('kat_icerik')->result();
  }else{
    return DB::orderBy('kategori','asc')->get('kat_icerik')->result();
  }
} 

/* START KATEGORİ OPTİON LİST */
public static function  yonetimKategori($id = 0,$secim = 0,$tire = 0){

  $sorgu = DB::where('kat_ustid',$id)->get('kategoriler')->result();

  foreach ($sorgu as $key) {

    if ($key->kat_ustid == 0) {$tire = 0;$style = 'color:#000;font-weight:bolder;font-size:13px;';}

    if ($secim != $key->kat_ustid){$secim = $key->kat_ustid;$style = 'color:#000';$tire++;}

    echo '<option value="'.$key->id.'" style="'.$style.'">'.str_repeat('-&nbsp;',$tire).$key->adi."</option>";

    yFunc::yonetimKategori($key->id,$secim,$tire);
  }
}
/* STOP KATEGORİ OPTİON LİST */

/* START WİKİ KATEGORİ OPTİON LİST */
public static function  wiki_kat($id = 0,$secim = 0,$tire = 0){
  $sorgu = DB::where('kat_ustid',$id)->get('wiki_kat')->result();
  foreach ($sorgu as $key) {
    if ($key->kat_ustid == 0) {
      $tire = 0;
      $style = 'color:#000;font-weight:bolder;font-size:13px;';
    }

    if ($secim != $key->kat_ustid){ 
      $secim = $key->kat_ustid;
      $style = 'color:#000';
      $tire++;
    }

    echo '<option value="'.$key->id.'" style="'.$style.'">'.str_repeat('-&nbsp;',$tire).$key->adi."</option>";
    yFunc::wiki_kat($key->id,$secim,$tire);
  }
}
/* STOP WİKİ KATEGORİ OPTİON LİST */

/* START FORUM KATEGORİ OPTİON LİST */
public static function  forum_kat($id = 0,$secim = 0,$tire = 0){
  $sorgu = DB::where('kat_ustid',$id)->get('forum_kat')->result();
  foreach ($sorgu as $key) {
    if ($key->kat_ustid == 0) {
      $tire = 0;
      $style = 'color:#000;font-weight:bolder;font-size:13px;';
    }

    if ($secim != $key->kat_ustid){ 
      $secim = $key->kat_ustid;
      $style = 'color:#000';
      $tire++;
    }

    echo '<option value="'.$key->id.'" style="'.$style.'">'.str_repeat('-&nbsp;',$tire).$key->adi."</option>";
    yFunc::forum_kat($key->id,$secim,$tire);
  }
}
/* STOP FORUM KATEGORİ OPTİON LİST */


public static function seo($s) {
  $tr = array('ş','Ş','ı','I','İ','ğ','Ğ','ü','Ü','ö','Ö','Ç','ç','(',')','/',':',',');
  $eng = array('s','s','i','i','i','g','g','u','u','o','o','c','c','','','-','-','-');
  $s = str_replace($tr,$eng,$s);
  $s = strtolower($s);
  $s = preg_replace('/&amp;amp;amp;amp;amp;amp;amp;amp;amp;.+?;/', '', $s);
  $s = preg_replace('/\s+/', '-', $s);
  $s = preg_replace('|-+|', '-', $s);
  $s = preg_replace('/#/', '', $s);
  $s = str_replace('.', '', $s);
  $s = trim($s, '-');
  return $s;
}

public static function menuname(){
  return DB::orderBy('menu_name','asc')->get('menuname')->result();
}

public static function yazilar(){
  return DB::orderBy('baslik','asc')->get('icerik')->result();
}


public static function kategoriler(){
  return DB::orderBy('kategori','asc')->get('kategori')->result();
}

public static function tcevir($tarih) {
  $bosluk = explode(' ', $tarih);
  $tr = explode("-",$bosluk['0']);
  $tarih1 = $tr['2']."-".$tr['1']."-".$tr['0'];
  return $tarih1;
}

public static function authority($data){
  return DB::select('authority')->where('id',$data)->get('authority')->value();
}

}
