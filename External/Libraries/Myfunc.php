<?php 
	/**
	* created by: Erkan IŞIK
	* created date: 2017-08-28
	*/
	class Myfunc{
		
		public static function loginControl(){
			if(!Session::select('login')){
				redirect(baseUrl().'panel/home/login');
			}
		}



		public static function kelimebol($metin, $karaktersayisi){ 
			$icerik = htmlspecialchars_decode($metin);
			$icerik = strip_tags($icerik);
			$icerik = str_replace(array("\t","\r","\n"), ' ',$icerik);
			$icerik_bol = explode(' ', $icerik); // metini bosluklara gore bolduk
			$icerik = '';
			for($i = 0; $i < count($icerik_bol); $i++) {
			     if ($icerik_bol[$i] != '') // veri yok mu? Atla, varsa ekle
			     $icerik .= trim($icerik_bol[$i]).' ';
			 }

			 if( preg_match('/(.*?)\s/i', substr($icerik, $karaktersayisi), $dizi) )
			 	return $icerik = substr($icerik, 0, $karaktersayisi+strlen($dizi[0]));
			} //kelimedenbol

			public static function katmenu($parent = '0') {
				$sql = DB::get('kat_icerik')->result();

				foreach($sql AS $key){
					$diziler[$key->id] = [
					'adi_seo'     => $key->adi_seo,
					'adi'  => $key->adi,
					'kat_ustid'  => $key->kat_ustid,
					'id'      => $key->id
					];
				}

				$has_childs = false;
				foreach($diziler as $key => $value)  {
					if ($value['kat_ustid'] == $parent) { 
						if ($has_childs === false){
							$has_childs = true;
							echo '<ul>';
						}
						echo '<li><a href="'.baseUrl().'category/'.$value['id'].'-'.$value['adi_seo'].'.html">'.$value['adi'].'</a>';
						Myfunc::katmenu($key);
						echo '</li>';
					}
				}
				if ($has_childs === true) echo "</ul>";
		}//katmenu

			public static function menu($parent = '0') {
				$sql = DB::get('kat_icerik')->result();

				foreach($sql AS $key){
					$diziler[$key->id] = [
					'adi_seo'     => $key->adi_seo,
					'adi'  => $key->adi,
					'kat_ustid'  => $key->kat_ustid,
					'id'      => $key->id
					];
				}

				$has_childs = false;
				foreach($diziler as $key => $value)  {
					if ($value['kat_ustid'] == $parent) { 
						if ($has_childs === false){
							$has_childs = true;							
						}
						echo '<a href="'.baseUrl().'category/'.$value['id'].'-'.$value['adi_seo'].'.html" class="btn btn-primary">'.$value['adi'].'</a>';
						Myfunc::menu($key);
						
					}
				}
				if ($has_childs === true) echo "</ul>";
		}//katmenu

		public static function myalert($data){
			echo '<script>alert("'.$data.'")</script>';
		}

		public static function tcevir($tarih) {
			$bosluk = explode(' ', $tarih);
			$tr = explode("-",$bosluk['0']);
			$saat = $bosluk['1']['0'].$bosluk['1']['1'].$bosluk['1']['2'].$bosluk['1']['3'].$bosluk['1']['4'];
			$tarih1 = $tr['2']."-".$tr['1']."-".$tr['0'].' '.$saat;
			return $tarih1;
		} 

		public static function tarih($tarih, $ayrac = '.') {

			$tr = explode($ayrac,$tarih);
			$tarih =  $tr['2']."-".$tr['1']."-".$tr['0'];
			return $tarih;
		}

		public static function editor($ed){
			return DB::select('username')->where('user_id',$ed)->get('user')->value();
		} 

		public static function setting(){
			return DB::get('settings')->row();
		}

	}//class sonu