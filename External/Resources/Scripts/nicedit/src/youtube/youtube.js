var nicYouTubeButton = nicEditorAdvancedButton.extend({
    width: '350px',

    addPane: function () {
        this.addForm({
            '': { type: 'title', txt: 'YouTube Url' },
            'youTubeUrl': { type: 'text', txt: 'URL', value: 'http://', style: { width: '150px'} },
            'height': { type: 'text', txt: 'Height', value: '560', style: { width: '150px'} },
            'width': { type: 'text', txt: 'Width', value: '315', style: { width: '150px'} }
        });
    },

    submit: function (e) {
        var code = this.inputs['youTubeUrl'].value;
        var width = this.inputs['height'].value;
        var height = this.inputs['width'].value;

        if (code.indexOf('watch?v=') > 0) {
            code = code.replace('watch?v=','embed/');
        }

        var youTubeCode = '<iframe width="' + width + '" height="' + height + '" src="' + code + '" frameborder="0" allowfullscreen></iframe>';

        this.ne.selectedInstance.setContent(this.ne.selectedInstance.getContent() + youTubeCode);
        this.removePane();
    }
});