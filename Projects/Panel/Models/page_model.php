<?php 
	/**
	* Created By: Erkan IŞIK
	* Created Date: 2017-09-05
	*/
	class Page_model extends model{
		



		function newPage($post){
			Upload::settings
			([
			    'encode'     => false,
			    'prefix'     => '_content_page_',
			    'extensions' => 'jpg|png|gif|jpeg ',
			   ])->target('upload')->start('resim');
			$baslik_seo = yFunc::seo($post['baslik']);
			DB::insert('content',[
				'icerik_resim' => Upload::info()->path,
				'icerik_baslik' => $post['baslik'] ,
				'baslik_seo' => $baslik_seo,
				'icerik_detay' => $post['yazi'],
				'icerik_keyword' => $post['keyword'],
				'icerik_durum' => '1',
				'icerik_tag' => $post['etiket'],
				'icerik_katid' => $post['kategori'] ,
				'mainpage' => $post['mainpage'],
				'editor' => Session::select('userid'),
				'icerik_tur' => 'sayfa',
			]);
			redirect(baseUrl('panel/page'));
		}

		

	}// class sonu