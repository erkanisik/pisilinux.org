<?php 
	/**
	* Created By  : Erkan IŞIK
	* Created Date: 2017-12-27
	* Update Date : 2017-12-27
	*/
	class WikiModel extends model{
		
		function new_category_save($post){
			$adi_seo = yFunc::seo($post['kategori']);
			
			Upload::settings([
			    'encode'     => false,
			    'prefix'     => 'wiki_cat_',
			    'extensions' => 'jpg|png|gif',			    
			])->target('upload')->start('resim');

			DB::insert('wiki_kat',[
				'adi' => $post['kategori'],
				'adi_seo' => $adi_seo,
				'kat_ustid' => $post['ustkategori'],
				'aciklama' => $post['aciklama'],
				'img' => Upload::info()->path,
				]);
			if (DB::affectedRows()) {redirect('panel/wiki/kategori');}
		}


		function new_content_save($post){
			Upload::settings
			([
			    'encode'     => false,
			    'prefix'     => '__content__',
			    'extensions' => 'jpg|png|gif',
			   ])->target('upload')->start('resim');
			$baslik_seo = yFunc::seo($post['baslik']);
			DB::insert('content',[
				'icerik_resim' => Upload::info()->path,
				'icerik_baslik' => $post['baslik'] ,
				'baslik_seo' => $baslik_seo,
				'icerik_detay' => $post['yazi'],
				'icerik_keyword' => $post['keyword'],
				'icerik_durum' => $post['durum'],
				'icerik_tag' => $post['etiket'],
				'icerik_katid' => $post['kategori'] ,
				'mainpage' => $post['mainpage'],
				'editor' => Session::select('userid'),
				'icerik_tur' => 'icerik',
			]);
			redirect(baseUrl('panel/wiki_content'));
		}

		

	}// class sonu