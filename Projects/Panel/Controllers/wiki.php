<?php 

/**
* Created By  : Erkan IŞIK
* Created Date: 2017-12-27
* Update Date : 2018-04-08
*/
class Wiki extends Controller{
	
	
		function main(){
			$data['yazilist'] = DB::orderBy('id','desc')->get('wiki')->result();

			import::view('wiki/content_list',$data);
		}

		function edit($id){
			if (method::post()) {$this->panel->new_content_update(method::post());}
			$data['edit'] = DB::where('icerik_id',$id)->get('content')->row();

			import::view('wiki/content_edit',$data);

		}

		function delete($id){
			DB::where('icerik_id', $id)->delete('content');
			redirect(baseUrl('panel/wiki'));
		}

		function new_content(){
			if (method::post()) {
				$this->panel->new_content_save(method::post());
			}
			import::view('wiki/new_content'); 
		}


		function kategori(){
			$data['katList'] = DB::where('kat_ustid','30')->get('wiki_kat')->result();
			import::view('wiki/wiki_kat_list',$data);
		}

		function new_category(){
			if (method::post()) {$this->wikiModel->new_category_save(method::post());}
			import::view('wiki/new_category');
		}


}