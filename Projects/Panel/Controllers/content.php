<?php 
	/**
	* Created By: Erkan IŞIK
	* Created Date: 2017-08-30
	* Upcdate Date: 2017-12-29
	*/
	class Content extends Controller{

		function main(){
			$data['yazilist'] = DB::where('icerik_tur','icerik')->orderBy('icerik_id','desc')->get('content')->result();

			import::view('content/content_list',$data);
		}

		function edit($id){
if (method::post()) {
				$this->panel->new_content_update(method::post());
			}
			$data['edit'] = DB::where('icerik_id',$id)->get('content')->row();

			import::view('content/content_edit',$data);

		}

		function delete($id){
			DB::where('icerik_id', $id)->delete('content');
			redirect(baseUrl('panel/content'));
		}

		function new_content(){
			if (method::post()) {
				$this->panel->new_content_save(method::post());
			}
			import::view('content/new_content');
		}


	}