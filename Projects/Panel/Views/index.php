<?php IMPORT::view('header'); ?>



  <div class="widget-box">
    <div class="widget-title"> <span class="icon"><i class="fa fa-th"></i></span>
      <h5>Yönetim Mesaj Alanı</h5>
    </div>
      <div class="widget-content nopadding">
          <table class="table table-bordered table-striped">
              
              <tbody>
                 <tr class="gradeX"><td><b>Anasayfa->anasayfada üye giriş kısmı</b> yapıldı. üye kayıt kısmında eklenecek parametre varsa bana iletin.</td></tr>
                  <tr class="gradeX"><td><b>Ayar Güncelleme->Logo ve Favicon yüklemesi</b> yapıldı</td></tr>
                  <tr class="gradeX"><td><b>slayt yönetimi->slayt listesi ve yeni slayt ekleme</b> yapıldı</td></tr>
                  <tr class="gradeX"><td><b>üye yönetimi->üye listesi</b> listelemesi yapıldı</td></tr>
                  <tr class="gradeX"><td><b>icerik->içerik listesi</b> listelemesi yapıldı, yazı giriş formu yapıldı, veritabanına kayıt kısmı yapılacak.</td></tr>
                  <tr class="gradeX"><td><b>LOGIN</b> girişi tamamen yapıldı, giriş yapılabiliyor ve üye şifresini değiştirebiliyor.</td></tr>
               
              </tbody>
          </table>
      </div>
  </div>

  <div class="widget-box">
    <div class="widget-title"> <span class="icon"> <i class="fa fa-bars"></i> </span>
      <h5>Mesaj listesi</h5>
     
    </div>
    <div class="widget-content nopadding">

      <table class="table table-bordered table-striped">
        <thead>
          <tr>
           <th>Tarih</th>
            <th>Gönderen</th>
            <th>Mail Adresi</th>
            <th>Açıklama</th>
            <th>cevap tarihi</th>
            
            
            <th >İşlemler</th>
          </tr>
        </thead>
        <tbody>
          <?php  foreach ($mesaj as $key) { ?>
          <tr class="odd gradeX">
            <td><?php echo $key->date; ?></td>
           
            <td><?php echo $key->isim; ?></td>
            <td><?php echo $key->email; ?></td>
            <td><?php echo mb_substr($key->mesaj, '0','100');
            //echo kelimebol($key->mesaj,100); ?></td>
              <td><?php echo $key->senddate; ?></td>
           
            <td  style="text-align:center;">
              <a href="<?php echo baseUrl('panel/iletisim/oku/'.$key->id); ?> "><button><i class="fa fa-edit"></i>OKU</button></a>
            </td>
          </tr>    
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>

<?php Import::view('footer'); ?>