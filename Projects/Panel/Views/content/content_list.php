<?php import::view('header'); ?>
<h1>Yazılar</h1>

<div class="widget-box">
    <div class="widget-title"> <span class="icon"> <i class="fa fa-bars"></i> </span>
      <h5>Yazı Listesi</h5>
      <div class="pull-right" style="margin:5px;"><a href="<?php echo baseUrl('panel/content/new_content') ?> "><button>YAZI EKLE</button></a></div>
    </div>
    <div class="widget-content nopadding">

      <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th width="100">Resim</th>
            <th>Başlık</th>
            <th>Açıklama</th>
            <th>Durum</th>
            <th>Etiket</th>
            <th>Keyword</th>
            <th>Düzenle</th>
            <th>Sil</th>
          </tr>
        </thead>
        <tbody>
          <?php  foreach ($yazilist as $key) { ?>
          <tr class="odd gradeX">
            <td><img src="<?php echo '../'.$key->icerik_resim ?>" alt="" width="100"> <br><?php echo Myfunc::tcevir($key->icerik_zaman); ?></td>
            <td><?php echo $key->icerik_baslik; ?></td>
            <td><?php echo Myfunc::kelimebol($key->icerik_detay,100); ?></td>
            <td><?php echo $key->icerik_durum == '1'? 'AKTİF':'PASİF'; ?></td>
            <td><?php echo $key->icerik_tag; ?></td>
            <td><?php echo $key->icerik_keyword; ?></td>
            <td>
            <a href="<?php echo baseUrl('panel/content/edit/'.$key->icerik_id); ?> "><button><i class="fa fa-edit"></i></button></a>
            </td>
            <td>
            <a href="<?php echo baseUrl('panel/content/delete/'.$key->icerik_id); ?> " onclick="return confirm('Bu kaydı silmek istediğinize eminmisiniz?')"><button><i class="fa fa-trash"></i></button></a></td>
          </tr>    
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
<?php import::view('footer'); ?>