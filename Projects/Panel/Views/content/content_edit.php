<?php IMPORT::view('header'); ?>
<div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="fa fa-align-justify"></i> </span>
          <h5>Yeni İçerik Ekleme Formu</h5>
        </div>
        <div class="widget-content nopadding">
     
       
          <form action="#" method="post" class="form-horizontal" enctype="multipart/form-data">
			
			<div class="control-group">
              <label class="control-label">KATEGORİSİ :</label>
              <div class="controls">
              <select name="kategori" id="" class="span11">

        <option value="">Yazının kategorinisi seçiniz...</option>

                  <?php echo KategoriListesi(0,0,0,$edit->icerik_katid); ?>
                  </select>
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label">RESİM :</label>
              <div class="controls">
                <input type="file" name="resim" >
                <input type="text" name="resim1" class="span11" value="" placeholder="Varsa resim linkini buraya yazabilirsiniz">
                <img src="<?php echo baseUrl().$edit->icerik_resim ?>" alt="" width="150">
              </div>
            </div>

            

             <div class="control-group">
              <label class="control-label">BAŞLIK :</label>
              <div class="controls">
                <input type="text" name="baslik" class="span11" value="<?php echo $edit->icerik_baslik ?>">
              </div>
            </div>


            
             <div class="control-group">
              <label class="control-label">YAZI :</label>
              <div class="controls">

                <textarea name="yazi" id="area2" rows="20" class="span11"><?php echo $edit->icerik_detay ?></textarea>
                <script>
            CKEDITOR.replace( 'yazi' );
        </script>
              </div>
            </div>

             

            <div class="control-group">
              <label class="control-label">ETİKETLER :</label>
              <div class="controls">
                <input type="text" name="etiket" class="span11" placeholder="Etiketleri aralarına virgül koyarak ekleyin..." value="<?php echo $edit->icerik_tag ?>">
              </div>
            </div>


            <div class="control-group">
              <label class="control-label">KISA AÇIKLAMA :</label>
              <div class="controls">
                <input type="text" name="keyword" class="span11" placeholder="Yazı hakkında kısa bir açıklama yazın..." value="<?php echo $edit->icerik_keyword ?>">
              </div>
            </div>


            <div class="control-group">
              <label class="control-label">DURUM :</label>
              <div class="controls">
                <select name="durum" id="" class="span11">
                	<option value="">SEÇİNİZ...</option>
                	<option value="1" <?php echo $edit->icerik_durum == '1'?'selected':''; ?>>AKTİF</option>
                	<option value="0" <?php echo $edit->icerik_durum == '0'?'selected':''; ?>>PASİF</option>
                </select>
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">ANASAYFADA YAYINLANSINMI :</label>
              <div class="controls">
                <select name="mainpage" id="" class="span11">
                	
                	<option value="1" <?php echo $edit->mainpage == '1'?'selected':''; ?>>EVET</option>
                	<option value="0" <?php echo $edit->mainpage == '0'?'selected':''; ?>>HAYIR</option>
                </select>
              </div>
            </div>
          
            <div class="form-actions">
              <input type="hidden" name="id" value="<?php echo $edit->icerik_id; ?>">
              <button type="submit" class="btn btn-success">Kaydet</button>
            </div>
          </form>
        </div>
      </div>
<?php Import::view('footer'); ?>