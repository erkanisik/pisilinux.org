<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="fa fa-home"></i> Yönetim Paneli</a>
  <ul>
    <li><a href="<?php echo baseUrl('panel'); ?> "><i class="fa fa-home"></i> <span>Yönetim Paneli</span></a> </li>
    
    <li class="submenu"><a href="#"><i class="fa fa-file"></i> <span>İçerik Yönetimi</span></a> 
      <ul>
        <li><a href="<?php echo baseUrl('panel/content'); ?>">İçerik Listesi</a></li>
        <li><a href="<?php echo baseUrl('panel/content/new_content'); ?>">İçerik Ekle</a></li>  
        <li><a href="<?php echo baseUrl('panel/category'); ?>">Kategori Yönetimi</a></li>     
      </ul>
    </li>
    <li><a href="<?php echo baseUrl('panel/forum') ?>"><i class="fa fa-file"></i>Forum Yönetimi</a></li>

    <li class="submenu"><a href="#"><i class="fa fa-file"></i> <span>Wiki Yönetimi</span></a> 
      <ul>
        <li><a href="<?php echo baseUrl('panel/wiki'); ?>">İçerik Listesi</a></li>
        <li><a href="<?php echo baseUrl('panel/wiki/new_content'); ?>">İçerik Ekle</a></li>  
        <li><a href="<?php echo baseUrl('panel/wiki/kategori'); ?>">Kategori Yönetimi</a></li>     
      </ul>
    </li>

    <li class="submenu"><a href="#"><i class="fa fa-user"></i> <span>Sayfa Yönetimi</span></a>
      <ul>
        <li><a href="<?php echo baseUrl('panel/page'); ?>">Sayfa Listesi</a></li>
        <li><a href="<?php echo baseUrl('panel/page/sayfaekle'); ?>">Sayfa Ekle</a></li>
      </ul>
    </li>

    <li><a href="<?php echo baseUrl('panel/slayt'); ?>"><i class="fa fa-picture-o"></i>Slayt Yönetimi</a></li>
         
    <li><a href="<?php echo baseUrl('panel/member'); ?>"><i class="fa fa-user"></i> <span>Üye Yönetimi</span></a></li>

    <li><a href="<?php echo baseUrl('panel/indir'); ?>"><i class="fa fa-cog"></i>İndir</a></li>
    <li><a href="<?php echo baseUrl('panel/setting'); ?>"><i class="fa fa-cog"></i>Site Ayarları</a></li>
    <li><a href="<?php echo baseUrl('panel/menu'); ?>"><i class="fa fa-cog"></i>Menü Yönetimi</a></li>
    <li><a href="<?php echo baseUrl('panel/iletisim'); ?>"><i class="fa fa-cog"></i>Site iletişim</a></li>


    <li><a href="<?php echo baseUrl('panel/funclist'); ?>">Fonksiyon Listesi</a></li>
   
  </ul>
</div>
<!--sidebar-menu-->
