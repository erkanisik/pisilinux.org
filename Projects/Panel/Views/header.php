<?php Myfunc::loginControl(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php //echo Myfunc::setting()->ayar_title; ?></title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<?php import::script('jquery321'); ?>
 <?php $d = date('dmYHis'); ?>
 <script src="<?php echo baseurl().ADMIN; ?>assets/js/myjsscript.js?v=<?php echo $d ?>"></script> 
<link rel="stylesheet" href="<?php echo baseurl().ADMIN ?>assets/css/bootstrap.min.css" />
<link rel="stylesheet" href="<?php echo baseurl().ADMIN ?>assets/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="<?php echo baseurl().ADMIN ?>assets/css/uniform.css" />
<link rel="stylesheet" href="<?php echo baseurl().ADMIN ?>assets/css/select2.css" />
<link rel="stylesheet" href="<?php echo baseurl().ADMIN ?>assets/css/datepicker.css" />
<link rel="stylesheet" href="<?php echo baseurl().ADMIN ?>assets/css/matrix-style.css" />
<link rel="stylesheet" href="<?php echo baseurl().ADMIN ?>assets/css/matrix-media.css" />
<link rel="stylesheet" href="<?php echo baseurl().ADMIN ?>assets/css/my.css?v=<?php echo $d ?>" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

<?php Import::view('editor'); ?>
<?php 
	import::style('google');
	import::style('awesome');
	import::style('jqueryui');
	//import::script('ckeditor');
 ?>
</head>
<body>

<!--Header-part-->
<div id="header">
  <h1><a href="<?php echo baseUrl(); ?>"><?php echo Myfunc::setting()->ayar_title; ?></a></h1>
</div>
<!--close-Header-part--> 
<?php 
	IMPORT::view('navbar'); 
  	Import::view('sidebarmenu'); 
?><div id="content">
<div id="content-header"> 
  <div id="breadcrumb"> </div>
</div>
<div class="container-fluid">
	<div class="row-fluid">