<?php import::view('header'); ?>
<!-- bluebox theme  -->
<div class="col-lg-12">
  <div class="panel panel-default">
    <div class="panel-heading">İçerik Ekle</div>
    <div class="panel-body">
      <div class="row">
        <div class="col-lg-12">
          <form role="form" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label>Wiki Kategori</label>
              <select name="kategori" id="" class="form-control">
                <option value=""><strong>Yazının kategorinisi seçiniz...</strong></option>
                <?php foreach (wiki_kategoriler('30') as $key): ?>
                  <option value="<?php echo $key->id ?>"><?php echo $key->adi ?></option>
                   <?php foreach (wiki_kategoriler($key->id) as $key1): ?>
                     <option value="<?php echo $key1->id ?>"><?php echo '-»'.$key1->adi ?></option>
                  <?php endforeach ?>
                <?php endforeach ?>
               
              </select>             
            </div>

            <div class="form-group">
              <label>Baslık</label>
              <input class="form-control" name="baslik" placeholder="Enter text">
            </div>

            <div class="form-group">
              <label>Resim</label>
              <input type="file" name="resim" class="form-control">
            </div>
            
            <div class="form-group">
                <label>Yazı</label>
                <textarea class="form-control" name="yazi" rows="20"></textarea>
                <script>
              CKEDITOR.replace( 'yazi', {});

            </script>
              </div>

              <div class="form-group">
              <label>Etiketler</label>
              <input class="form-control" name="etiket" placeholder="Enter text">
            </div>

            <div class="form-group">
              <label>Kısa Açıklama</label>
              <input class="form-control" name="keyword" placeholder="Enter text">
            </div>
                    
              <button type="submit" class="btn btn-default">KAYDET</button>
              
            </form>
        </div>
        </div>
        </div>
      </div>
    <!--  -->
   
<?php import::view('footer'); ?>