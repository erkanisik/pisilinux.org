<?php IMPORT::view('header'); ?>

<h1>İndirme Yöneticisi</h1>

<div class="widget-box">
    <div class="widget-title"> <span class="icon"> <i class="fa fa-bars"></i> </span>
      <h5>İndir Listesi</h5>
      <div class="pull-right" style="margin:5px;"><a href="<?php echo baseUrl('panel/indir/yeni') ?> "><button>+</button></a></div>
    </div>
    <div class="widget-content nopadding">

      <table class="table table-bordered table-striped">
        <thead>
          <tr>
           <th>Başlık</th>
           <th>Açıklama</th>
            <th>Link</th>
            <th>MD5SUM</th>
            <th>SHA1SUM</th>
             <th width="80">İşlem</th>
          </tr>
        </thead>
        <tbody>
          <?php  foreach ($list as $key) { ?>
          <tr class="odd gradeX">            
            <td><?php echo $key->baslik; ?></td>
            <td><?php echo $key->aciklama; ?></td>
            <td><?php echo $key->link; ?></td>
            <td><?php echo $key->md5sum; ?></td>
            <td><?php echo $key->sha1sum; ?></td>
            
            <td>
            <a href="<?php echo baseUrl('panel/indir/edit/'.$key->id); ?> "><button><i class="fa fa-edit"></i></button></a>
            <a href="<?php echo baseUrl('panel/indir/delete/'.$key->id); ?> " onclick="return confirm('Bu kaydı silmek istediğinize eminmisiniz?')"><button><i class="fa fa-trash"></i></button></a>
            </td>
           
          </tr>    
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>

<?php Import::view('footer'); ?>