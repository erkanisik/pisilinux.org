<?php import::view('header');?>
  <div class="widget-box">
    <div class="widget-title"> <span class="icon"><i class="fa fa-th"></i></span>
      <h5>Üyeler</h5>
      <div class="pull-right" style="margin:3px;"><a href="<?php echo baseUrl('panel/member/usernew') ?>"><button class="btn btn-primary">Yeni Üye Ekle</button></a></div>
    </div>
      <div class="widget-content nopadding">
          <table class="table table-bordered data-table" >
              <thead>
                <tr>
                  <th width="100">KAYIT TARİHİ</th>
                  <th width="50">AVATAR</th>
                  <th>ÜYE ADI</th>
                  <th>MAİL ADRESİ</th>
                  <th width="50">YETKİSİ</th>
                  <th width="50">DURUMU</th>
                  <th>AKTİVİZASYON KODU <br>( Kodu tekrar göndermek için kodun üstüne tıklayın ) </th>
                  <th width="100">SON GİRİŞ TARİHİ</th>
                  
                  <th width="100">İŞLEMLER</th> 
                </tr>
              </thead>
              <tbody>
                <?php foreach ($member as $key): ?>
                   <tr class="gradeX">
                  <td><?php echo Myfunc::tcevir($key->date); ?></td>
                  <td><img src="<?php echo '../'.$key->avatar; ?>" alt="" width="50"></td>
                  <td><?php echo $key->username; ?></td>
                  <td><?php echo $key->mail; ?></td>
                  <td><?php echo Yfunc::authority($key->authority); ?></td>
                  <td style="text-align: center"><?php echo $key->status == '1'? '<i class="fa fa-check"></i>':'<i class="fa fa-close"></i>'; ?></td>
                  <td><p class="active1" data-id="<?php echo $key->aktivizasyon; ?>" ><?php echo $key->aktivizasyon; ?></p></td>
                  <td style="text-align: center"><?php if ($key->login_date) {echo Myfunc::tcevir($key->login_date);}  ?></td>

                  <td style="text-align: center">
                    <a href="<?php echo baseUrl('panel/member/mail/'.$key->user_id); ?> "><button><i class="fa fa-envelope"></i></button></a>
                    <a href="<?php echo baseUrl('panel/user/edit'.$key->user_id); ?> "><button><i class="fa fa-edit"></i></button></a>
                    <a href="<?php echo baseUrl('panel/user/delete/'.$key->user_id); ?>" onclick="return confirm('Bu kaydı silmek istediğinize eminmisiniz?')"><button><i class="fa fa-trash"></i></button></a>
                  </td>
                </tr>
                <?php endforeach ?>
               
               
              </tbody>
          </table>
      </div>
  </div>


<?php import::view('footer'); ?>