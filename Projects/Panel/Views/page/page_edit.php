<?php import::view('header'); ?>
<div class="widget-box">
    <div class="widget-title"> <span class="icon"> <i class="fa fa-align-justify"></i> </span>
    	<h5>Sayfa Düzenleme Formu</h5>
	</div>
    <div class="widget-content nopadding">
    	<form action="#" method="post" class="form-horizontal" enctype="multipart/form-data">
    		<div class="control-group">
              <div class="span3">
                <label class="control-label">Sayfa resmi :</label>
                <div class="controls">
                  <img src="<?php echo baseUrl($pages->icerik_resim); ?>" alt="<?php echo $pages->icerik_baslik; ?>" title="<?php echo $pages->icerik_baslik; ?>" width="150">
                </div>
              </div>
              <div class="span9">
                <label class="control-label">Resim seç :</label>
                <div class="controls">
                  <input type="file" name="logo" class="span11" placeholder="Logo Yükle" />
                </div>
              </div>
            </div>
            <div class="control-group">
            	<label class="control-label">Başlık :</label>
            	<div class="controls">
                	<input type="text" name="baslik" value="<?php echo $pages->icerik_baslik; ?>" class="span11" required>
                	<!-- <input type="text" name="baslik" value="<?php echo seo($pages->icerik_baslik); ?>" class="span11" > -->
              	</div>
            </div>            
            <div class="control-group">
            	<label class="control-label">Sayfa :</label>
              	<div class="controls">
					         <textarea name="yazi" id="area2" rows="20" class="span11"><?php echo $pages->icerik_detay; ?></textarea>
	                 <script>
	            		   CKEDITOR.replace( 'yazi' );
	        		     </script>
            	</div>
            </div>
            <div class="control-group">
            	<label class="control-label">Etiketler :</label>
            	<div class="controls">
               		<input type="text" name="etiket" value="<?php echo $pages->icerik_tag; ?>" class="span11" placeholder="Etiketleri aralarına virgül koyarak ekleyin..." required>
              	</div>
            </div>
            <div class="control-group">
            	<label class="control-label">Kısa açıklama :</label>
              	<div class="controls">
                	<input type="text" name="keyword" value="<?php echo $pages->icerik_keyword; ?>" class="span11" placeholder="Yazı hakkında kısa bir açıklama yazın..." required>
              	</div>
            </div>
            <div class="control-group">
            	<label class="control-label">Durum :</label>
	            	<div class="controls">
      						<select name="durum" id="" class="span11">
      							<option value="1" <?php echo $pages->icerik_durum = '1'?'selected':''; ?> >Aktif</option>
      							<option value="0" <?php echo $pages->icerik_durum = '0'?'selected':''; ?> >Pasif</option>
      						</select>
	              </div>
            </div>
            <div class="form-actions">
            	<button type="submit" class="btn btn-success">Düzenle</button>
            </div>
        </form>
    </div>
</div>
<?php import::view('footer'); ?>