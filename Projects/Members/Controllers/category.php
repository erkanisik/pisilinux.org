<?php 

/**
	* Created By: Erkan IŞIK
	* Created Date: 2017-09-05
	*/
class Category extends Controller{
	
	function main()
	{
		import::view('category/category_list_view');
	}

	function new_category(){
		if (method::post()) {$this->panel->new_category_save(method::post());}
		import::view('category/category_new_view');	
	}

	function delete($id){
		DB::where('id', $id)->delete('kategoriler');
		redirect(baseUrl('panel/category'));
	}
}