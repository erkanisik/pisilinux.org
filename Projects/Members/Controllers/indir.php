<?php 
/*
* Created By: Erkan IŞIK
* Created Date: 2017-12-07
* Update Date: 2017-12-07
*/
	class Indir extends Controller{
		
		function main(){
			$data['list'] = DB::get('download')->result();
			import::view('indir/index',$data);			
		}

		function yeni(){
			if (method::post()){$this->indir_model->yeni(method::post());}
			import::view('indir/yeni');			
		}
	}