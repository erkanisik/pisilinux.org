<?php 
	/**
	* Created By  : Erkan IŞIK
	* Created Date: 2017-08-30
	* Update Date : 2018-01-01
 	*/
	class Content extends Controller{

		function main(){

			$data['yazilist'] = DB::where('icerik_tur','icerik','and')->where('editor', Session::select('userid'))->orderBy('icerik_id','desc')->get('content')->result();

			import::view('content/content_list',$data);
		}

		function edit($id){

			$data['conten'] = DB::where('icerik_id',$id)->get('content')->row();

			import::view('content/content_edit',$data);

		}

		function delete($id){
			DB::where('icerik_id', $id,'and')->where('editor',Session::select('userid'))->delete('content');
			redirect(baseUrl('members/content'));
		}

		function new_content(){
			if (method::post()) {
				$this->members_model->new_content_save(method::post());
			}

			import::view('content/new_content');
		}


	}