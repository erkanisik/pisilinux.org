<?php 
/**
* 
*/
class Page extends Controller{

	function main(){
		$data['sayfalist'] = DB::where('icerik_tur','sayfa')->orderBy('icerik_id','desc')->get('content')->result();
		import::view('page/index',$data);
	}

	function sayfaekle(){
	if(method::post()){
		$this->page_model->newPage(method::post());
		}
		import::view('page/new');
	}
	function edit($id){
		$data['pages'] = DB::where('icerik_id',$id)->get('content')->row();
		import::view('page/page_edit',$data);
	}

	function delete($id){
		DB::where('icerik_id', $id)->delete('content');
		redirect(baseUrl('panel/page/index'));
	}

}// class sonu

