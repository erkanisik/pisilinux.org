<?php 
/*
* Created By: Erkan IŞIK
* Created Date: 2017-12-07
* Update Date: 2017-12-07
*/
class Indir_model extends Model
{
	
	function yeni($post){
		DB::insert('download',[
			'baslik' => $post['baslik'],
			'aciklama' => $post['yazi'],
			'link' => $post['link'],
			'md5sum' => $post['md5sum'],
			'sha1sum' => $post['sha1sum']
		]);
		if (DB::affectedRows()) {
			redirect(baseUrl('panel/indir'));
		}
	}
}