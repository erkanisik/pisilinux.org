<?php 
	/**
	* Created By: Erkan IŞIK
	* Created Date: 2018-01-01
	* Update Date: 2018-05-10
	*/
	class Members_model extends model{
		
		function new_category_save($post)
		{
			$adi_seo = yFunc::seo($post['kategori']);
			DB::insert('kategoriler',[
				'adi' => $post['kategori'],
				'adi_seo' => $adi_seo,
				'kat_ustid' => $post['ustkategori']
				]);
			if (DB::affectedRows()) {redirect('members/category');}
		}


		function new_content_save($post){
			Upload::settings([
			    'encode'     => false,
			    'prefix'     => '__content__',
			    'extensions' => 'jpg|png|gif',			    
			   ])->target('upload')->start('resim');
			$baslik_seo = yFunc::seo($post['baslik']);
			

			DB::insert('content',[
				'icerik_resim' => Upload::info()->path,
				'icerik_baslik' => $post['baslik'] ,
				'baslik_seo' => $baslik_seo,
				'icerik_detay' => $post['yazi'],
				'icerik_keyword' => $post['keyword'],
				'icerik_durum' => '0',
				'icerik_tag' => $post['etiket'],
				'icerik_katid' => $post['kategori'] ,
				'mainpage' => '0',
				'editor' => Session::select('userid'),
				'icerik_tur' => 'icerik',
			]);
			redirect(baseUrl('members/content'));
			
		}

		

		
	}// class sonu