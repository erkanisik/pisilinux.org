 <!--/. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
    <div id="sideNav" href=""><i class="fa fa-caret-right"></i></div>
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

                    <li><a href="<?php echo baseUrl() ?>members"><i class="fa fa-dashboard"></i>Üye Anasayfa</a></li>
                    <li><a href="<?php echo baseUrl() ?>"><i class="fa fa-dashboard"></i>Site Anasayfa</a></li>
                    <li><a href="<?php echo baseUrl() ?>forum"><i class="fa fa-dashboard"></i>Forum</a></li>
                    <li><a href=""><i class="fa fa-user"></i> Profilim<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="<?php echo baseUrl(); ?>members/user"><i class="fa fa-user"></i>Güncelle</a></li>
                            <li><a href="<?php echo baseUrl(); ?>members/user/passrename"><i class="fa fa-key"></i>Şifremi Değiştir</a></li>
                            <li><a href="<?php echo baseUrl(); ?>members/home/logout"><i class="fa fa-sign-out"></i>Çıkış</a></li>                      
                        </ul>
                    </li>
                    <?php if (Session::select('yetki') == '3'): ?>
                        <li><a href="<?php echo baseUrl(); ?>members/content"><i class="fa fa-bars"></i>İçerik Yöneticisi</a></li>
                    <li><a href="<?php echo baseUrl(); ?>members/wiki"><i class="fa fa-bars"></i>Wiki Yöneticisi</a></li>
                    <?php endif ?>

                    
                    <li><a href="<?php echo baseUrl(); ?>members/user/logout"><i class="fa fa-sign-out"></i>Çıkış</a></li>
                   
                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->