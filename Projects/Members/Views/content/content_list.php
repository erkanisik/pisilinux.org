<?php import::view('header'); ?>


<!-- liste  -->
<div class="col-md-12">
 <div class="panel panel-default">
  <div class="panel-heading">
    Yazılarım
     <div class="pull-right" style="margin:-3px;"><a href="<?php echo baseUrl('members/content/new_content') ?> "><button>YAZI EKLE</button></a></div>
  </div>
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table">
        <thead>
          <tr>
            <th>#</th>
            
            <th>Başlık</th>
            <th>Yayınlama</th>
            <th>Anasayfa</th>
            
            
            <th>Kategori</th>
            <th>Yazar</th>
            <th>Tarih</th>
            <th>Hit</th>
            <th>İşlemler</th>
          </tr>
        </thead>
        <tbody>
          <?php $a = ''; foreach ($yazilist as $key) { $a = $a+1; ?>
          <tr>
            <td><?php echo $a; ?></td>
            <td><?php echo $key->icerik_baslik; ?></td>
            <td><?php echo $key->icerik_durum == '1'? 'EVET':'HAYIR'; ?></td>
            <td><?php echo $key->mainpage == '1'? 'EVET':'HAYIR'; ?></td>
            
            
            <td><?php echo katname($key->icerik_katid); ?></td>
            <td><?php echo editor($key->editor)->username; ?></td>
            <td><?php echo tcevir($key->icerik_zaman); ?></td>
            <td><?php echo $key->hits; ?></td>
            <td>
              <a href="<?php echo baseUrl('members/content/edit/'.$key->icerik_id); ?> "><button><i class="fa fa-edit"></i></button></a>
              <a href="<?php echo baseUrl('members/content/delete/'.$key->icerik_id); ?> " onclick="return confirm('Bu kaydı silmek istediğinize eminmisiniz?')">
                <button><i class="fa fa-trash-o"></i></button>
              </a>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
<!--  -->
  <?php import::view('footer'); ?>