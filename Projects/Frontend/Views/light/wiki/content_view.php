<?php import::view(TEMA.'header'); ?>

<section id="Page-title" class="Page-title-Style1">
    <div class="container inner-Pages">
        <div class="row">
            <div class="Page-title">
                <div class="col-md-6 Title-Pages">
                    <h2>Pisi Linux Wiki </h2>
                </div>
                <div class="col-md-6 Catogry-Pages">
                    <p>Buradasınız :  <a href="<?php baseUrl(); ?>">Anasayfa</a> / Wiki / <?php echo $icerik->baslik; ?></p>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="Latest-News" class="light-wrapper">
    <div class="container inner">
        <div class="row">
        <!-- sol menü -->
        <?php import::view(TEMA.'wiki/left_blok'); ?>

        <!-- /sol menü -->
    
            <div class="col-md-8">
                <div class="Title-Single">
                    <div class="Top-Title-Blog">
                        <h2><?php echo $icerik->baslik; ?></h2> 
                    </div>
                    <div class="Bottom-Title-Blog">
                        <ul>
                            <li>
                                <i class="fa fa-user"></i>
                                <p>Ekleyen : <a href="#"><?php echo editor($icerik->editor); ?></a></p>
                            </li>
                            <li><i class="fa fa-clock-o"></i><p>Tarih : <?php echo tcevir($icerik->zaman); ?></p></li>
                            <li><i class="fa fa-folder-open"></i><p>Kategori : <a href="#"><?php echo katname($icerik->katid); ?></a></p></li>
                            
                        </ul>
                    </div>
                </div>
                <div class="Single-Blog-icerik">
                    <div class="Post-Images"> 
                        <img src="<?php echo baseUrl($icerik->resim); ?>" alt="<?php echo $icerik->baslik; ?>"  style="width: 500px;">
                    </div>
                    <div class="Post-icerik">
                        <p> 
                            <?php echo $icerik->detay ?>
                        </p>
                    </div>
                </div> 

                <div class="divcod50"></div>
                   <div class="Under-Post">
                   
                    <div id="shareIconsCount" class="Social-icerik Social-Blog jssocials">
                        <div class="jssocials-shares">
                            <div class="jssocials-share jssocials-share-facebook">
                                <a target="_blank" href="https://facebook.com/sharer/sharer.php?u=<?php echo baseUrl('icerik/'.$icerik->id.'-'.$icerik->baslik_seo.'.html'); ?>" class="jssocials-share-link">
                                    <i class="fa fa-facebook jssocials-share-logo"></i>
                                </a>
                                <div class="jssocials-share-count-box jssocials-share-no-count">
                                    <span class="jssocials-share-count"></span>
                                </div>
                            </div>
                            <div class="jssocials-share jssocials-share-twitter">
                                <a target="_blank" href="https://twitter.com/share?url=<?php echo baseUrl('icerik/'.$icerik->id.'-'.$icerik->baslik_seo.'.html'); ?>" class="jssocials-share-link">
                                    <i class="fa fa-twitter jssocials-share-logo"></i>
                                </a><div class="jssocials-share-count-box jssocials-share-no-count"><span class="jssocials-share-count"></span></div></div>

                                <div class="jssocials-share jssocials-share-googleplus"><a target="_blank" href="https://plus.google.com/share?url=<?php echo baseUrl('icerik/'.$icerik->id.'-'.$icerik->baslik_seo.'.html'); ?>" class="jssocials-share-link"><i class="fa fa-google-plus jssocials-share-logo"></i></a><div class="jssocials-share-count-box jssocials-share-no-count"><span class="jssocials-share-count"></span></div></div>

                                <div class="jssocials-share jssocials-share-linkedin"><a target="_blank" href="https://www.linkedin.com/shareArticle?url=<?php echo baseUrl('icerik/'.$icerik->id.'-'.$icerik->baslik_seo.'.html'); ?>" class="jssocials-share-link"><i class="fa fa-linkedin jssocials-share-logo"></i></a><div class="jssocials-share-count-box jssocials-share-no-count"><span class="jssocials-share-count"></span></div></div>

                                <div class="jssocials-share jssocials-share-pinterest">
                                    <a target="_blank" href="https://pinterest.com/pin/create/bookmarklet/?&amp;url=<?php echo baseUrl('icerik/'.$icerik->id.'-'.$icerik->baslik_seo.'.html'); ?>&amp;description=<?php echo Myfunc::kelimebol($icerik->detay,100); ?>" class="jssocials-share-link"><i class="fa fa-pinterest jssocials-share-logo"></i></a><div class="jssocials-share-count-box jssocials-share-no-count"><span class="jssocials-share-count"></span></div></div></div>

                        </div>
                </div>
                <div class="divcod50"></div>

            </div>
        </div>
    </div>
</section>
<?php import::view(TEMA.'footer'); ?>