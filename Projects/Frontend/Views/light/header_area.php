<!-- End Sidebar  -->
<div class="content-wrap">
	<div id="home" class="body-wrapper">
		<!-- Start Section Header style3 -->
		<section class="Header-Style3">
			<div class="TopHeader">
				<div class="container">
					<div class="row">
						<div class="Contact-h col-md-6">
							<div class="Social-Footer">
								<ul class="icons-ul">
									<li class="facebook">
										<a href="https://www.facebook.com/Pisilinux/" target="_blank"><span class="fa fa-facebook hvr-icon-up"></span></a>
									</li>
									<li class="twitter">
										<a href="https://twitter.com/PisiLinux" target="_blank"><span class="fa fa-twitter hvr-icon-up"></span></a>
									</li>
									<li class="google">
										<a href="https://plus.google.com/+PisiLinuxGNU" target="_blank"><span class="fa fa-google-plus hvr-icon-up"></span></a>
									</li>
									<li class="youtube">
										<a href="https://www.youtube.com/channel/UCYR3ICNwAE4gwZRV5QmGAqw" target="_blank"><span class="fa fa-youtube-play hvr-icon-up"></span></a>
									</li>
									<li class="linkedin">
										<a href="https://www.linkedin.com/in/pisi-linux-33a5b6137" target="_blank"><span class="fa fa-linkedin hvr-icon-up"></span></a>
									</li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-6">
                          	<div class="Link-ul">
                          		<ul class="icons-ul">
                                
                                <?php if (session::select('yetki')): ?>
                                  <li class="loginuser"><a href="#"><span>Merhaba: <?php echo session::select('username'); ?></span></a></li>
                                <?php endif ?>
                                
                          			<li><a href="<?php echo baseUrl('forum') ?>"><span>Forum</span></a></li>
                                <li ><a href="<?php echo baseUrl('blog') ?>"><span>Blog</span></a></li>
                                <li ><a href="<?php echo baseUrl('indir') ?>"><span>İNDİR</span></a></li>
                                <li ><a href="<?php echo baseUrl('wiki') ?>"><span>Wiki</span></a></li>
                                <li ><a href="<?php echo baseUrl('hata_kaydi') ?>"><span>Hata Kaydı</span></a></li>
                           		</ul>
                           	</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Start Navbar -->
            <div class="Navbar-Header navbar basic" data-sticky="true">
              	<div class="container">
               		<div class="row">
               			<div class="Logo-Header col-md-4">
               				<a class="navbar-logo" href="<?php echo baseUrl(); ?> ">
               					<img src="<?php echo baseUrl('upload/Logo-Header.png') ?>">
               				</a>
               			</div>
               			<!-- 
               			<div class="right-wrapper">
               				<div class="Icons-Search">
               					<a href="#"><i class="fa fa-search"></i></a>
               				</div>
               				<div class="Block-Search">
               					<input id="m_search" name="s" type="text" placeholder="Aramak istediğiniz kelimeyi yazın ve giriş tuşuna basın...">
               					<button><i class="fa fa-search"></i></button>
               				</div>
               			</div>
								-->
               			<div class="collapse pull-right navbar-collapse">

               				<div id="cssmenu" class="Menu-Header top-menu">
               					<ul>
              						<li><a href="<?php echo baseUrl(); ?>">Anasayfa</a></li>
                          <li><a href="<?php echo baseUrl() ?>page/43-hakkimizda.html">Hakkımızda</a></li>
                          <li><a href="<?php echo baseUrl() ?>iletisim">İLETİŞİM</a></li>
                          <?php if (Session::select('yetki')) { ?>
                            <li><a href="<?php echo baseUrl(); ?>members">PANELİM</a></li>
                            <li><a href="<?php echo baseUrl() ?>uye/logout">ÇIKIŞ</a></li>


                            <?php }else{ ?>
                          <li><a href="<?php echo baseUrl('uye'); ?> ">GİRİŞ YAP</a></li> 
                          <li><a href="<?php echo baseUrl('uye/yeniuye'); ?>">ÜYE OL</a></li>  
                          <?php } ?>       						
              				  </ul>
               				</div>
                      
               			</div>
               		</div>
               	</div>
            </div>
                <!-- End Navbar -->
        </section>

