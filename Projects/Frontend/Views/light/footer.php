

<!-- Start Footer -->

<section id="footer-4">
	<div class="container inner-f">
   <div class="row">
     <div class="Footer-Blocks">

       <div class="col-md-4">
         <div class="Top-Block-Footer">
           <h2>Diğer Sitelerimiz</h2>
         </div>
         <div class="divcod20"></div>
         <div class="Contact-Footer">
          <ul>
           <li><a href="https://pisilinux.github.io/deutsche.webseite.io/index.html" target="_blank">Pisilinux Almanya</a></li>
           <li><a href="https://pisilife.wordpress.com/" target="_blank">pisilinux ispanya</a></li>
           <li><a href="https://pisilinux.github.io/developer.pisilinux.io/" target="_blank">Pisi Linux Developer Base</a></li>
           <li><a href="https://github.com/pisilinux/main/graphs/contributors" target="_blank">Pisilinux Team</a></li>

         </ul>
       </div>
     </div>




     <div class="col-md-4">
       <div class="Contact-Footer">
        <ul>
         <li><a href="https://goo.gl/4UiVe4" target="_blank">Sunucu Sponsorumuz Turhost <br> <img src="<?php echo baseUrl('img/sponsor/sunucu-sponsoru-turhost.png') ?>" alt="" width="200"></a></li>
         <li></li>
         <li></li>
       </ul>
     </div>
   </div>



   <div class="col-md-4">
     <div class="Top-Block-Footer">
      <h2>Bizi takip edin</h2>
    </div>
    <div class="divcod20"></div>
    <div class="Bottom-Block-Footer">

      <div class="Social-join">
        <div class="Social-Footer">
          <ul class="icons-ul">
           <li class="facebook"><a href="https://www.facebook.com/Pisilinux/" target="_blank"><span class="fa fa-facebook hvr-icon-up"></span></a></li>
           <li class="twitter"><a href="https://twitter.com/PisiLinux" target="_blank"><span class="fa fa-twitter hvr-icon-up"></span></a></li>
           <li class="google"><a href="https://plus.google.com/+PisiLinuxGNU" target="_blank"><span class="fa fa-google-plus hvr-icon-up"></span></a></li>
           <li class="youtube"><a href="https://www.youtube.com/channel/UCYR3ICNwAE4gwZRV5QmGAqw"><span class="fa fa-youtube-play hvr-icon-up"></span></a></li>
           <li class="linkedin"><a href="https://www.linkedin.com/in/pisi-linux-33a5b6137"><span class="fa fa-linkedin hvr-icon-up"></span></a></li>
         </ul>
       </div>
     </div>

   </div>
 </div>


</div>
</div>
</div>
</section>
<!-- 
-->
<footer class="footer">
  <section id="Bottom-Footer" >
   <div class="container inner-f">
     <div class="row">
       <div class="col-md-6">
         <div class="Rights-Reserved">
           <h2>All Rights Reserved © 2017. <a href="<?php echo baseurl(); ?>"><span>PisiLinux</span></a></h2>
         </div>
       </div>
       <div class="col-md-6 text-right">
         <div class="Link-Footer">
           <ul>
             <li><a href="<?php echo baseUrl(); ?>">Anasayfa</a></li>
             <li><a href="#">Blog</a></li>
             <li><a href="#">İndir</a></li>
             <li><a href="#">Hata Kaydı</a></li>

             <li><a href="#">İletişim</a></li>
           </ul>
         </div>
       </div>
     </div>
   </div>
 </section>
</footer>
<!-- End Footer -->

</div>
   <?php $er = rand(); ?>
<!-- Back to top Link -->
<div id="to-top" class="main-bg"><span class="fa fa-chevron-up"></span></div>
</div>

<!-- Placed at the end of the document so the pages load faster -->

<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/jquery-1.11.3.min.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/bootstrap.min.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/menu-script.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<?php import::script('nicedit/nicEdit.js'); ?>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/rs-plugin/js/extensions/revolution.extension.video.min.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/rs-plugin/js/extensions/revolution.extension.actions.min.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/rs-plugin/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/rs-plugin/js/extensions/revolution.extension.migration.min.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?> style/rs-plugin/js/extensions/revolution.extension.parallax.min.js"></script>

<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/rev_custom.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/instafeed.min.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/isotope.pkgd.min.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/jquery.easing.min.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/jquery.elevateZoom-3.0.8.min.js"></script>   
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/jquery.fancybox.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/jquery.fancybox.pack.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/jquery.fancybox-media.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/jquery.inview.min.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/jflickrfeed.min.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/tweetie.min.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/instgram_custom.js"></script>

<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/modernizr.custom.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/classie.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/main.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/owl.carousel.min.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/jquery.counterup.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/waypoints.min.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/wow.min.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/custom.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/responsiveslides.min.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/bottom.js"></script>
<script src="<?php echo baseUrl(TEMA_DIR); ?>style/js/my.js?v=<?php echo $er; ?>"></script>


</body>

</html>
