﻿<?php 
/*
* Created By: Erkan IŞIK
* Created Date: 2017-05-09
* Update Date: 2017-12-29
*/
	import::view(TEMA.'header'); 
	include'slider.php';

?>

<style>
	a:hover, a:focus{
		text-decoration: none;;
	}
</style>

<section id="Welcome" class="light-wrapper">
	<div class="container inner">
    	<div class="row">
        	<div class="col-md-12">
                <div class="title-section text-center">
                    <h3>Pisi Linux'a Hoş Geldiniz!</h3>
                </div>
                <div class="description-welcome text-center">
                    <p>Pisi Linux, özgür yazılım topluluğu tarafından geliştirilen, son kullanıcı odaklı bir GNU/Linux dağıtımıdır.</p>
                </div>
            </div>
        </div>
        <div class="divcod30"></div>
    	<div class="row">
			<div class="col-md-4">
				<div class="welcome-Block text-center">
					<div class="Top-welcome">
						<i class="icon icon-Tablet"></i>
						<h4>Son teknolojiyi kullanır.</h4>
					</div>
					<div class="Bottom-welcome">
						<p>Mevcut paketlerin en son kararlı sürümlerini bulundurur. Türkçe’yi tam ve doğru olarak kullanır. Yazım ve kelime denetimleri yaparak sizin doğru, düzgün ve güzel bir Türkçe kullanmanızı sağlar.</p>
						
					</div>
				</div>
            </div>
			<div class="col-md-4">
				<div class="welcome-Block text-center">
					<div class="Top-welcome">
						<i class="icon icon-Tie"></i>
						<h4>Hızlı ve kolay kurulur.</h4>
					</div>
					<div class="Bottom-welcome">
						<p>Kurulum işlemi bir çok bilgisayarda hızlı kurulur size vakit kaybettirmez ve grafiksel ekran sayesinde kurulumu kolaylıkla yapabilirsiniz. Kurulumdan sonra ofis yazılımı, İnternet gezgini, sohbet programı gibi gerekli bütün programlar da yüklenir.</p>
						
					</div>
				</div>
            </div>
			<div class="col-md-4">
				<div class="welcome-Block text-center">
					<div class="Top-welcome">
						<i class="icon icon-Starship"></i>
						<h4>Özgürdür.</h4>
					</div>
					<div class="Bottom-welcome">
						<p>Özgürdür ve açık kaynaklıdır. Kodlarına istediğiniz gibi erişip istediğiniz değişikliği yapabilirsiniz. Virüs derdi yoktur. İnternet’ten indirdiğiniz dosyaları kontrol etmek ile uğraşmazsınız.</p>
						
					</div>
				</div>
            </div>

        </div>
    </div>
</section>

<!-- End Welcome To Yamen -->

<!-- Start About Us -->
<section id="about" class="whitesmoke-wrapper">
	<div class="container inner">
    	<div class="row">
        	<div class="col-md-12">
                <div class="title-section text-center">
                    <h3>Hakkımızda</h3>
                    <div class="line-break"></div>
                </div>
                <div class="description-section text-center">
                    <p>Pisi Linux, Pisi tabanlı, özgür yazılım topluluğu tarafından geliştirilen, bilgisayar kulanıcılarına kurulum, yapılandırma ve kullanım konusunda kolaylık sağlamaya çalışan, onların temel masaüstü gereksinimlerini karşılamayı amaçlayan, son kullanıcı odaklı bir GNU/Linux dağıtımıdır.</p>
                </div>
            </div>
        </div>
        <div class="divcod30"></div>

    </div>
</section>
<!-- End About Us -->

<!-- Start Great Features -->
<section id="Features-Us" class="color-wrapper parallax Features-parallax">
	<div class="wrapper-twitter"></div>
	<div class="container inner">
    	<div class="row">
        	<div class="col-md-12">
                <div class="title-section-Features text-center">
                    <h3>PİSİ LİNUX ÖZELLİKLERİ</h3>
                    <div class="line-break-Features"></div>
                </div>
                <div class="description-section-Features text-center">
                    <p>Hızlı ve kararlı yapısıyla ve kullanıcı dostu pisilinux aileden biri gibidir. Kullandıkça seveceğiniz, vazgeçemeyeceğiniz ve gerçek türkçe dil desteği olan tek dağıtımdır.</p>
                </div>
            </div>
        </div>
        <div class="divcod30"></div>
		<div class="row">
			<div class="cd-tadbs col-md-12 text-center">
				<nav>
					<ul class="tabs-one col-md-2 tabs-navigation">
						<li>
							<div class="Block-Features text-center selected">
								<a data-content="design" href="#0"><i class="fa fa-paint-brush"></i><h6>Tamdır…</h6></a>
							</div>
						</li>
						<li>
							<div class="Block-Features text-center">
								<a data-content="options" href="#0"><i class="fa fa-cogs"></i><h6>Virüslere geçit vermez…</h6></a>
							</div>
						</li>
						<li>
							<div class="Block-Features text-center">
								<a data-content="speed" href="#0"><i class="fa fa-rocket"></i><h6>Hızlı ve kolay kurulur…</h6></a>
							</div>
						</li>
					</ul>
					<ul class="tabs-navigation col-md-2 tabs-tow">
						<li>
							<div class="Block-Features text-center">
								<a data-content="responsive" href="#0"><i class="fa fa-tablet"></i><h6>Türkçe sever…</h6></a>
							</div>
						</li>
						<li>
							<div class="Block-Features text-center">
								<a data-content="features" href="#0"><i class="fa fa-thumbs-up"></i><h6>Özelleştirilebilir…</h6></a>
							</div>
						</li>
						<li>
							<div class="Block-Features text-center">
								<a data-content="support" href="#0"><i class="fa fa-smile-o"></i><h6>Kolaydır…</h6></a>
							</div>
						</li>
					</ul> <!-- tabs-navigation -->
				</nav>
				<div class="divcod30"></div>
				<ul class="tabs-content">
					<li data-content="design" class="selected col-md-6">
						<div class="block-tabs-content">
							<div class="col-md-6">
								<h2>Tamdır</h2>
								<p>Pisi Linux’u kurduktan sonra geniş bir uygulama yelpazesi ile sizi karşılar. Eğer kurulu gelen uygulamalar arasında aradığınız uygulamalar bulunmuyorsa, depolarında bulunan yüzlerce paketlerden birisi mutlaka işinizi görecektir. Ayrıca donanımınızı kolaylıkla tanımaktadır.
								</p>							
							</div>
							<div class="col-md-6">
								<img src="<?php echo baseUrl(TEMA_DIR); ?>style\images\services\12.png" alt="about images" width="570" height="296">
							</div>
						</div>
					</li>

					<li data-content="options" class="col-md-6">
						<div class="block-tabs-content">
							<div class="col-md-6">
								<h2>Virüslere geçit vermez…</h2>
								<p>Linux alt yapsından aldığı güçle, Pisilinux'da virüslerin sisteminizi bozmasına asla izin vermez.</p>
								
							</div>
							<div class="col-md-6">
								<img src="<?php echo baseUrl(TEMA_DIR); ?>style\images\services\Features-Tap-2.png" alt="about images" width="570" height="296">
							</div>
						</div>
					</li>

					<li data-content="speed" class="col-md-6">
						<div class="block-tabs-content">
							<div class="col-md-6">
								<h2>Hızlı ve kolay kurulur…</h2>
								<p> Hiç olmadığı kadar kısa sürede kurulumunu yapabilir. Kurulum sonrası sürücü kurmak ile uğraşmaz, bilgisayarınızın keyfinizi sürersiniz.</p>
								
							</div>
							<div class="col-md-6">
								<img src="<?php echo baseUrl(TEMA_DIR); ?>style\images\services\Features-Tap-3.jpg" alt="about images" width="570" height="296">
							</div>
						</div>
					</li>

					<li data-content="responsive" class="col-md-6">
						<div class="block-tabs-content">
							<div class="col-md-6">
								<h2>Türkçe sever…</h2>
								<p>PisiLinux ana dilimizi tamamıyla desteklemek için yoğun çaba gösterir. Depolardaki paketlerin Türkçe desteğine özen gösterir. Türkçe desteği vermeyen paketler için çeviri çalışmaları da yürütür.</p>
								
							</div>
							<div class="col-md-6">
								<img src="<?php echo baseUrl(TEMA_DIR); ?>style\images\services\Features-Tap-4.jpg" alt="about images" width="570" height="296">
							</div>
						</div>
					</li>

					<li data-content="features" class="col-md-6">
						<div class="block-tabs-content">
							<div class="col-md-6">
								<h2>Özelleştirilebilir…</h2>
								<p>Pisilinux'ta KDE ile gelen KAPTAN sayesinde aklınızın sınırlarını aşan sayıda sınırsız kombinasyon ile bilgisayarınızı özelleştirebilir. Siz hayal edin KAPTAN yapsın</p>
								
							</div>
							<div class="col-md-6">
								<img src="<?php echo baseUrl(TEMA_DIR); ?>style\images\services\Features-Tap-5.jpg" alt="about images" width="570" height="296">
							</div>
						</div>
					</li>

					<li data-content="support" class="col-md-6">
						<div class="block-tabs-content">
							<div class="col-md-6">
								<h2>Kolaydır…</h2>
								<p>PisiLinux'u kullanmak, hiç ummadağınız kadar kolay! Size sunulan ile yetinmeyin, hayal edebildiğiniz kadar geliştirin, kolaylıkla kullanın!</p>
								
							</div>
							<div class="col-md-6">
								<img src="<?php echo baseUrl(TEMA_DIR); ?>style\images\services\Features-Tap-6.jpg" alt="about images" width="570" height="296">
							</div>
						</div>
					</li>
				</ul> <!-- tabs-content -->
			</div> <!-- tabs -->
        </div>
    </div>
</section>
<!-- End Great Features -->

<!-- Start TXT with Video -->
<section id="Features-text" class="whitesmoke-wrapper">
	<div class="container inner">
        <div class="row">
        	<div class="col-md-6">
            	<div class="Features-Block">
                	<h3>Hızlı kararlı ve kullanıcı dostu!</h3>
                    <div class="line-break"></div>
                    <p>İlk defa pisi linux kullanan kullanıcılar için KDE masaüstü ortamı ön tanıtım videosu</p>
                    <a class="btn-bck" href="#">Devamı...</a>
                    <!--<a class="btn-notback" href="#">Features</a>!-->
                </div>
            </div>
        	<div class="col-md-6">
            	<div class="Block-Img">
            		<iframe width="560" height="315" src="https://www.youtube.com/embed/BN4Tjx-kCps?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                	
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End TXT with Video -->

<!-- Start Latest News -->
<section id="Latest-News" class="light-wrapper">
	<div class="container inner">
    	<div class="row">
        	<div class="col-md-12">
                <div class="title-section text-center">
                    <h3>Blogdan kısa kısa</h3>
                    <div class="line-break"></div>
                </div>
                <div class="description-section text-center">
                    <p>Bloğumuza sizler için eklediğimiz ve yararlı olacağını umduğumuz yazılarımız</p>
                </div>
            </div>
        </div>
        <div class="divcod30"></div> 
        <div class="row">
    		<div class="Top-News">
    		<?php foreach ($content as $key): ?>
    		<div class="block-News text-center col-md-4">
               <div class="Top-img-news">
                 <img src="<?php echo $key->icerik_resim; ?>" alt="blog" style="width: 293px; height: 205px;">
               </div>
               <div class="Bottom-title-news">
                  <div class="data-news text-left">
                    <p>On: <?php echo tcevir($key->icerik_zaman); ?> | Kategori: <a href="#"><?php echo katname($key->icerik_katid); ?></a></p>
                  </div>
                  <div class="title-news text-left">
                      <a href="<?php echo baseUrl('blog/icerik/'.$key->icerik_id.'-'.$key->baslik_seo.'.html'); ?>" style="text-decoration: none;"><h2><?php echo $key->icerik_baslik; ?></h2></a>
                      <p><?php echo Myfunc::kelimebol($key->icerik_detay,250); ?> </p>
                  </div>
                  <div class="Get-news text-left">
                       <a href="<?php echo baseUrl('content/'.$key->icerik_id.'-'.$key->baslik_seo.'.html'); ?>">Devamı</a>
                  </div>
                </div>
            </div>
        	<?php endforeach ?>
       <!--
             <div class="block-News text-center col-md-4">
                <div class="Top-img-news">
                  <img src="<?php echo baseUrl(TEMA_DIR) ?>style\images\blog\news-2.png" alt="blog" width="293" height="205">
                </div>
                <div class="Bottom-title-news">
                   <div class="data-news text-left">
                     <p>On: Feb 17, 2015 | Category: <a href="#">News</a></p>
                   </div>
                   <div class="title-news text-left">
                       <a href="#"><h2>Blog Title Goes Here</h2></a>
                       <p>Duis dapibus aliquam mi, eget euismod sem scelerisque ut. Vivamus at elit quis urna adipiscing iaculis vitae velit in neque dictum blandit, Proin in iaculis neque scelerisque ut. </p>
                   </div>
                   <div class="Get-news text-left">
                        <a href="#">Read More</a>
                   </div>
                 </div>
              </div>

              <div class="block-News text-center col-md-4">
                 <div class="Top-img-news">
                   <img src="<?php echo baseUrl(TEMA_DIR) ?>style\images\blog\news-3.png" alt="blog" width="293" height="205">
                 </div>
                 <div class="Bottom-title-news">
                    <div class="data-news text-left">
                      <p>On: Feb 17, 2015 | Category: <a href="#">News</a></p>
                    </div>
                    <div class="title-news text-left">
                        <a href="#"><h2>Blog Title Goes Here</h2></a>
                        <p>Duis dapibus aliquam mi, eget euismod sem scelerisque ut. Vivamus at elit quis urna adipiscing iaculis vitae velit in neque dictum blandit, Proin in iaculis neque scelerisque ut. </p>
                    </div>
                    <div class="Get-news text-left">
                         <a href="#">Read More</a>
                    </div>
                  </div>
               </div>
				!-->
            </div>
        </div>
    </div>
</section>
<!-- End Latest News -->

<!-- Start Contact 
<section id="Contact" class="light-wrapper"> 
	<div class="container inner">
    	<div class="row">
        	<div class="col-md-12">
                <div class="title-section text-center">
                    <h3>İLETİŞİM</h3>
                    <div class="line-break"></div>
                </div>
                <div class="description-section text-center">
                    <p>Pisi linux hakkında her türlü düşüncenizi sorularınızı form aracılığıyla ekibimize ulaştırabilirsiniz...</p>
                </div>
            </div>
        </div>
        <div class="divcod30"></div>
        
        <div class="row">

			<div class="col-md-12">
				<div class="Contact-Form">
					<form class="leave-comment contact-form" method="post" action="">
						<div class="Contact-us">

							<div class="form-input">
								<input type="text" name="name" placeholder="*Adınız" required>
							</div>

							<div class="form-input">
								<input type="email" name="email" placeholder="*E-Posta Adresiniz" required>
							</div>

							<div class="form-textarea">
								<textarea class="txt-box textArea" name="mesaj" cols="40" rows="7" id="messageTxt" placeholder="*iletmek istediğiniz mesajınız" spellcheck="true" required></textarea>
							</div>

							<div class="form-submit">
								<input type="hidden" name="islem" value="iletisim">
								<input type="submit" class="btn btn-large main-bg" value="Gönder">
							</div>

						</div>
					</form>
				</div>
			</div>
	
        </div>
    </div>
</section>-->
<!-- End Contact -->

<!-- Start Newsletter -->
<section id="Newsletter" class="whitesmoke-wrapper">
	<div class="container inner">
    	<div class="row">
        	<div class="col-md-12">
                <div class="title-section text-center">
                    <h3>E-Bülten</h3>
                    <div class="line-break"></div>
                </div>
                <div class="description-section text-center">
                    <p>E-Bültene abone olarak, pisilinux gelişmelerinden herkesden önce sizin haberiniz olsun.</p>
                </div>
            </div>
        </div>
        <div class="row">
			<div id="mc_embed_signup" class="Join-Newsletter">
				<form action="" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
					<input type="email" value="" placeholder="Abonelik için E-posta adresinizi yazın" name="EMAIL" class="txt-box required email" id="mce-EMAIL" style="width:69%;">
					<input type="submit" value="KAYDET" name="subscribe" id="mc-embedded-subscribe" class="button btn btnjoin">
				</form>
            </div>
        </div>
    </div>
</section>
<!-- End Newsletter -->

<?php import::view(TEMA.'footer'); ?>