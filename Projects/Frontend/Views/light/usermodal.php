<!-- Modal -->
<div id="login" class="modal fade" role="dialog" style="z-index: 999999;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Üye Girişi</h4>
      </div>
      <div class="modal-body">
         <div class="login-controls">
                <div class="input-box">
                    <input type="text" class="txt-box " name="username" placeholder="Email Adresiniz">
                </div>
                <div class="input-box">
                    <input type="password" class="txt-box " name="password" placeholder="Şifreniz">
                </div>
                <div class="main-bg">
                    <input type="submit" class="btn " value="Login">
                </div>
                <div class="check-box">
                    <a href="#">Şifremi Unuttum?</a>
                </div>

            </div>
      </div>
     
    </div>

  </div>
</div>

<style type="text/css">
  .txt-box{
    color:#000 !important;
  }
</style>
<!-- Modal -->
<div id="newuser" class="modal fade" role="dialog" style="z-index: 999999;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Yeni üye kayıt formu</h4>
      </div>
      <div class="modal-body">

         <div class="login-controls">
            <form method="post" enctype="multipart/form-data">
              <div class="input-box">
                   <label>Adınız Soyadınız</label><br>
                    <input type="text" class="txt-box " name="adisoyadi" required >
                </div>

                <div class="input-box">
                     <label>E-Posta Adresiniz</label><br>
                    <input type="text" class="txt-box " name="eposta" required>
                </div>

                <div class="input-box">
                   <label>Şifreniz</label><br>
                    <input type="password" class="txt-box " name="sifre" required>
                </div>

                <div class="input-box">
                   <label>Şifrenizi tekrar yazın</label><br>
                    <input type="password" class="txt-box " name="sifre2" required>
                </div>

                 <div class="input-box">
                  <label>Avatar</label><br>
                    <input type="file" name="avatar" class="span11">
                </div>

                <div class="main-bg">
                    <input type="hidden" name="islem" value="newuser">
                    <input type="submit" class="btn " value="Kayıt Ol">
                </div>
               <div class="check-box">
                    <a href="#" data-toggle="modal" data-target="#login" data-dismiss="modal">Zaten Üyeyim</a>
                </div>
            </div>

            </form>
      </div>
     
    </div>

  </div>
</div>