<?php import::view(TEMA.'header'); ?>

<section id="Page-title" class="Page-title-Style1">
    <div class="container inner-Pages">
        <div class="row">
            <div class="Page-title">
                <div class="col-md-6 Title-Pages">
                    <h2>Pisi Linux İçerik </h2>
                </div>
                <div class="col-md-6 Catogry-Pages">
                    <p>Buradasınız :  <a href="<?php baseUrl(); ?>">Anasayfa</a> / İçerik / <?php echo $icerik->icerik_baslik; ?></p>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="Latest-News" class="light-wrapper">
    <div class="container inner">
        <div class="row">
        <!-- sol menü -->
        <?php import::view(TEMA.'blog/blogleft'); ?>

        <!-- /sol menü -->
    
            <div class="col-md-8">
                <div class="Title-Single">
                    <div class="Top-Title-Blog">
                        <h2><?php echo $icerik->icerik_baslik; ?></h2> 
                    </div>
                    <div class="Bottom-Title-Blog">
                        <ul>
                            <li>
                                <i class="fa fa-user"></i>
                                <p>Yazar : <a href="#"><?php echo editor($icerik->editor); ?></a></p>
                            </li>
                            <li><i class="fa fa-clock-o"></i><p>Tarih : <?php echo tcevir($icerik->icerik_zaman); ?></p></li>
                            <li><i class="fa fa-folder-open"></i><p>Kategori : <a href="#"><?php echo katname($icerik->icerik_katid); ?></a></p></li>
                            <!--<li><i class="fa fa-comment"></i><p>Comments : <a href="#">31</a></p></li>-->
                            <li><i class="fa fa-eye"></i><p>Okunma : <a href="#"><?php echo $icerik->hits; ?></a></p></li>
                        </ul>
                    </div>
                </div>
                <div class="Single-Blog-icerik">
                    <div class="Post-Images"> 
                        <img src="<?php echo baseUrl($icerik->icerik_resim); ?>" alt="<?php echo $icerik->icerik_baslik; ?>"  style="width: 500px;">
                    </div>
                    <div class="Post-icerik">
                        <p> 
                            <?php echo $icerik->icerik_detay ?>
                        </p>
                    </div>
                </div> 

                <div class="divcod50"></div>
                   <div class="Under-Post">
                    <div class="tags">
                        <i class="fa fa-tags"></i><?php echo etiket($icerik->icerik_tag); ?>
                    </div>
                    <div id="shareIconsCount" class="Social-icerik Social-Blog jssocials">
                        <div class="jssocials-shares">
                            <div class="jssocials-share jssocials-share-facebook">
                                <a target="_blank" href="https://facebook.com/sharer/sharer.php?u=<?php echo baseUrl('icerik/'.$icerik->icerik_id.'-'.$icerik->baslik_seo.'.html'); ?>" class="jssocials-share-link">
                                    <i class="fa fa-facebook jssocials-share-logo"></i>
                                </a>
                                <div class="jssocials-share-count-box jssocials-share-no-count">
                                    <span class="jssocials-share-count"></span>
                                </div>
                            </div>
                            <div class="jssocials-share jssocials-share-twitter">
                                <a target="_blank" href="https://twitter.com/share?url=<?php echo baseUrl('icerik/'.$icerik->icerik_id.'-'.$icerik->baslik_seo.'.html'); ?>" class="jssocials-share-link">
                                    <i class="fa fa-twitter jssocials-share-logo"></i>
                                </a><div class="jssocials-share-count-box jssocials-share-no-count"><span class="jssocials-share-count"></span></div></div>

                                <div class="jssocials-share jssocials-share-googleplus"><a target="_blank" href="https://plus.google.com/share?url=<?php echo baseUrl('icerik/'.$icerik->icerik_id.'-'.$icerik->baslik_seo.'.html'); ?>" class="jssocials-share-link"><i class="fa fa-google-plus jssocials-share-logo"></i></a><div class="jssocials-share-count-box jssocials-share-no-count"><span class="jssocials-share-count"></span></div></div>

                                <div class="jssocials-share jssocials-share-linkedin"><a target="_blank" href="https://www.linkedin.com/shareArticle?url=<?php echo baseUrl('icerik/'.$icerik->icerik_id.'-'.$icerik->baslik_seo.'.html'); ?>" class="jssocials-share-link"><i class="fa fa-linkedin jssocials-share-logo"></i></a><div class="jssocials-share-count-box jssocials-share-no-count"><span class="jssocials-share-count"></span></div></div>

                                <div class="jssocials-share jssocials-share-pinterest">
                                    <a target="_blank" href="https://pinterest.com/pin/create/bookmarklet/?&amp;url=<?php echo baseUrl('icerik/'.$icerik->icerik_id.'-'.$icerik->baslik_seo.'.html'); ?>&amp;description=<?php echo Myfunc::kelimebol($icerik->icerik_detay,100); ?>" class="jssocials-share-link"><i class="fa fa-pinterest jssocials-share-logo"></i></a><div class="jssocials-share-count-box jssocials-share-no-count"><span class="jssocials-share-count"></span></div></div></div>

                        </div>
                </div>
                <div class="divcod50"></div>

            </div>
        </div>
    </div>
</section>
<?php import::view(TEMA.'footer'); ?>