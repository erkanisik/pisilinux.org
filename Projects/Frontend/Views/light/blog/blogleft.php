		<!-- sol menü -->
		<div class="col-md-4">
			<div class="SideBar-Blog">
				
				<div class="Categories-Blog">
					<div class="Top-Title-Page">
						<h3>Kategoriler</h3>
						<div class="line-break"></div>
					</div>
					<div class="Categories-Block">
						
						<ul>

							<?php foreach (kategoriler() as $key ): ?>
								<li><a href="<?php echo baseUrl('blog/cat/'.$key->id.'-'.$key->adi_seo.'.html'); ?>"><p><?php echo $key->adi; ?></p></a><i class="fa fa-angle-double-right"></i></li>
							<?php endforeach ?>
							
							
						</ul>
					
					</div>
				</div>

				<div class="Line-Bloge"></div>
				<a href="https://goo.gl/EWMG1i" target="_blank"><img src="<?php echo baseUrl('img/sponsor/cloud-server-turhost-banner-350x250px.png') ?>" alt="cloud server turhost banner"></a>
				<div class="Line-Bloge"></div>

				<div class="Tags-Blog">
					<div class="Top-Title-Page">
						<h3>Etiketler</h3>
						<div class="line-break"></div>
					</div>
					<div class="tags">
						<ul>
							<?php foreach (etiketler() as $key): ?>
							<li><a href="<?php echo baseUrl('etiketler/'.$key.'.html') ?>"><?php echo $key ?></a></li>	
							<?php endforeach ?>
							
							
						</ul>
					</div>
				</div>
				<div class="Line-Bloge"></div>
				<!-- 
				<div class="Latest-Tweets-Blog">
					<div class="Top-Title-Page">
						<h3>Son Tweetler</h3>
						<div class="line-break"></div>
					</div>
					<div id="tweets-footer" class="tweet query footer">
						<ul>
							<li>
								<i class="fa fa-twitter"></i> 
									<a href="https://twitter.com/EnvatoStudio" target="_blank" title="EnvatoStudio on Twitter">@EnvatoStudio</a> Envato Studio is presently undergoing a brief period of scheduled maintenance and will be back online soon.
									 <div class="date">May. 19, 2016</div>
							</li>
							<li>
								<i class="fa fa-twitter"></i> 
								<a href="https://twitter.com/EnvatoStudio" target="_blank" title="EnvatoStudio on Twitter">@EnvatoStudio</a> Envato Studio is currently undergoing some scheduled maintenance and will be back online shortly. <div class="date">Feb. 16, 2016</div>
							</li>
						</ul>
					</div>
				</div>
				 -->
			</div>
		</div>

		<!-- /sol menü -->
