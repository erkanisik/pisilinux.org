<?php import::view(TEMA.'header'); ?>
<section id="Page-title" class="Page-title-Style1">
	<div class="container inner-Pages">
    	<div class="row">
            <div class="Page-title">
				<div class="col-md-6 Title-Pages">
                	<h2>Pisi Linux Blog </h2>
                </div>
                <div class="col-md-6 Catogry-Pages">
					<p>Buradasınız :  <a href="<?php baseUrl(); ?>">Anasayfa</a>   /   Blog </p>
                </div>
            </div>
 		</div>
    </div>
</section>

<section id="Latest-News" class="light-wrapper">
<div class="container inner">
	<div class="row">
<?php import::view(TEMA.'blog/blogleft'); ?>
		<!-- içerik menü -->

		<div class="col-md-8">
			<div class="Top-News">
			
			<?php foreach ($content as $key ): ?>
				<div class="block-News text-center col-md-12">
				
				<div class="Top-img-news">
						<img src="<?php echo baseUrl().$key->icerik_resim; ?>" alt="blog" style="width: 500px;">
					<div class="data-news text-left">

						
                        <p>By:<a href="#"><?php echo editor($key->editor); ?></a> | On: <?php echo tcevir($key->icerik_zaman); ?> | kategori: <a href="#"><?php echo katname($key->icerik_katid); ?></a> | Tags: <a href="#">business</a>, <a href="#">videos</a></p>
					</div>
				</div>
				
				<div class="Bottom-title-news wide-blog">
					<div class="title-news text-left">
						<a href="<?php echo baseUrl('blog/icerik/'.$key->icerik_id.'-'.$key->baslik_seo.'.html'); ?>"><h2><?php echo $key->icerik_baslik; ?></h2></a>
						<p><?php echo Myfunc::kelimebol($key->icerik_detay,250); ?></p>
					</div>
					<div class="Get-news text-left">
						<a href="<?php echo baseUrl('blog/icerik/'.$key->icerik_id.'-'.$key->baslik_seo.'.html'); ?>">Devamı...</a>
					</div>
				</div>
				</div>

<?php endforeach ?>
			




	
			<!-- Start Pagination -->
			<div id="Pagination" class="light-wrapper Pagination-Full">
				<div class="row">
					<div class="pager col-md-12 text-center">
						<ul>
							<li class="selected"><span class="">1</span></li>
							<li><a href="#">2</a></li>
							<li><a href="#"><i class="fa fa-long-arrow-right"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- End Pagination -->

		</div>

	</div>
	<!-- içerik menü -->
</div>
</div>
</section>
<?php import::view(TEMA.'footer'); ?>