<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE10">
    <meta name="description" content="<?php echo ayar()->ayar_description; ?>">
    <meta name="keywords" content="<?php echo ayar()->ayar_keywords; ?>">
    <meta name="author" content="<?php echo ayar()->ayar_author; ?>">
    <link rel="shortcut icon" href="<?php echo baseUrl('img/fav/pisi16x16.ico'); ?>">
    <title><?php if(defined('TITLE')){echo ayar()->ayar_title.TITLE;}else{echo ayar()->ayar_title;} ?></title>
    <?php $d = rand(); ?>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo baseUrl(TEMA_DIR); ?>style/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo baseUrl(TEMA_DIR); ?>style/css/style.css?v=<?php echo $d; ?>" rel="stylesheet">
    <link href="<?php echo baseUrl(TEMA_DIR); ?>style/css/my.css?v=<?php echo $d; ?>" rel="stylesheet">
    <link href="<?php echo baseUrl(TEMA_DIR); ?>style/css/color/blue.css" rel="stylesheet">
    <link href="<?php echo baseUrl(TEMA_DIR); ?>style/css/forum.css?v=<?php echo $d; ?>" rel="stylesheet">
    <link href="<?php echo baseUrl(TEMA_DIR); ?>style/css/tabs.css?v=<?php echo $d; ?>" rel="stylesheet">
   

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="style/js/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
    
</head>
<body>

    <!-- Start Loading -->

    <section class="loading-overlay">
        <div class="Loading-Page">
          <h2 class="loader">Yükleniyor...</h2>
      </div>
  </section>

  <!-- End Loading  -->

  <!-- Start Sidebar  -->
  <?php include'header_area.php'; ?>
