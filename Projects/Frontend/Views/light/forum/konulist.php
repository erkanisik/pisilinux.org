<?php import::view(TEMA.'header'); ?>

<!-- start nerdeyim bloğu -->
<section id="Page-title" class="Page-title-Style1">
	<div class="container inner-Pages">
   <div class="row">
    <div class="Page-title">
      <div class="col-md-6 Title-Pages">
       <h2>Pisi Linux Forum </h2>
     </div>
     <div class="col-md-6 Catogry-Pages">
       <p>Buradasınız :  <a href="<?php echo baseUrl(); ?>">Anasayfa</a> / <a href="<?php echo baseUrl(); ?>forum">Forum</a> / <?php echo forum_kategori_adi($kategori); ?> </p>
     </div>
   </div>
 </div>
</div>
</section>
<!-- stop nerdeyim bloğu  -->

<!-- start FORUM BLOG -->
<section id="Forum" class="light-wrapper">
	<div class="container inner">
		<div class="row">
			<a href="<?php echo baseUrl('forum') ?> "><button class="btn btn-primary" style="margin-bottom: 5px;"> GERİ </button></a> 
<?php if (Session::select('userid')) { ?>
	
	<a href="<?php echo baseUrl('forum/yenikonu/').$kategori.'-'.forum_kategori_adi($kategori); ?>">
		<button class="btn btn-primary" style="margin-bottom: 5px;"><i class="fa fa-plus"></i> Yeni Konu Aç</button></a>
		
	<?php }else{ ?><!-- 
<button class="btn btn-info" style="margin-bottom: 5px;" ><a href="<?php echo baseUrl('uye/ref/forum-yenikonu-').$kategori.'-'.forum_kategori_adi($kategori); ?>" style="color:#fff;">KONU AÇABİLMEK İÇİN GİRİŞ YAP</a></button> -->


	<?php } ?>
			
				<div class="panel panel-info">
			  <div class="panel-heading"><?php echo forum_kategori_adi($kategori); ?></div>
			
			  	<table class="table td">
					<?php if ($konular) { ?>
					<?php foreach ($konular as $key): ?>
							<tr>
			  			<td><img src="<?php echo baseUrl('upload/forumico/pisiforum1.jpg') ?>" alt="" style="width: 70px;float: left;margin-right: 10px;">
			  				<p class="forum_baslik"><a href="<?php echo baseUrl('forum/konu/'.$key->id.'-'.$key->title_seo ) ?>"><?php echo $key->title; ?></a></p>
			  				<p class="forum_aciklama"><?php echo kelimebol($key->content,100); ?></p>
			  					
			  			</td>
			  			<td width="150" style="text-align: center; vertical-align: middle;color:#2e2e2e; font-size: 11px;">
			  				<strong><?php echo contentlist($key->id)+1; ?></strong> İleti<br>
			  				
			  			<td width="300" style=" font-size: 11px;color:#2e2e2e;"><strong>Son İleti:</strong> 
			  				<?php echo sonileti($key->id); ?>


			  			</td>
			  		</tr>
						
					<?php endforeach ?>
				
					<?php }else{ ?>

						<tr>
							<td colspan="3">
								Bu Konuya hiç ileti yazılmamış!...
							</td>
						</tr>

			

					<?php } ?>
					
			  	
			  	</table>
			</div>				
			
		</div>
	</div>
</section>
<!-- stop FORUM BLOG -->
<?php import::view(TEMA.'footer'); ?>