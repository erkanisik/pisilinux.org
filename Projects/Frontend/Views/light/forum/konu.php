<?php import::view(TEMA.'header'); ?>
<!-- start nerdeyim bloğu -->
<section id="Page-title" class="Page-title-Style1">
	<div class="container inner-Pages">
   <div class="row">
    <div class="Page-title">
      <div class="col-md-6 Title-Pages">
       <h2>Pisi Linux Blog </h2>
     </div>
     <div class="col-md-6 Catogry-Pages">
       <p>Buradasınız :  <a href="<?php echo baseUrl(); ?>">Anasayfa</a> / Forum </p>
     </div>
   </div>
 </div>
</div>
</section>
<!-- stop nerdeyim bloğu  -->

<!-- start FORUM BLOG -->
<section id="Forum" class="light-wrapper">
	<div class="container inner">

    <a href="<?php echo baseUrl('forum/konulist/').$fk->category_id.'-'.forum_kategori_adi($fk->category_id); ?>"><button class="btn btn-primary"> GERİ </button></a>
  <hr>
		<div class="row">
      <table class="table table-bordered forum_konu">
        <tr class="forum_title">
          <td width="150">GÖNDEREN</td>
          <td>KONU: <?php echo $fk->title; ?><!--  ( Okunma Sayısı: <?php echo $fk->view; ?>)  -->
            <div class="pull-right">#1</div>  
          </td>
        </tr>
        <tr>
          <td>
            <p class="yazar"><?php echo yazar($fk->user_id); ?></p>
           
            <?php if (avatar($fk->user_id)){ ?>
             <img src="<?php echo baseUrl(avatar($fk->user_id)); ?>" alt="" width="140" height="140">  
            
            <?php }else{ ?>
             <img src="<?php echo baseUrl(); ?>img/user.png" alt="" width="140" height="140">  
            <?php } ?>

          
            
          </td>
          <td>
            <p class="konu_baslik"><?php echo $fk->title; ?> <br>  
              <kp class="konu_tarih">« <?php echo tcevir($fk->insertDate,'1'); ?> »</kp>

            </p>
            <p class="konu_mesaj"> <?php echo $fk->content; ?></p>
          </td>
        </tr>
        <tr class="forum_title">
          <td colspan="2">
            <div class="pull-right" style="color:#fff;">
              <?php if(Session::select('userid') == $fk->user_id){?>
                <a href="/forum/konu_duzenle/<?php echo $fk->id; ?>" class="btn btn-default btn-sm">Düzenle</a> 
                <a href="#" data-id="<?php echo (int)$fk->id; ?>"  class="btn btn-default btn-sm konusil">Sil</a>
              <?php } ?>
            </div>
          </td>
        </tr>
      </table>

  <!-- cevapları sıralıyoruz -->
     <?php 
     $a=''; 
     foreach (cevaplist($fk->id) as $key ): 
      $a = (int)$a + 1; ?>
       <table class="table table-bordered forum_konu" data-id="<?php echo $key->id; ?>">
        <tr class="forum_title">
          <td width="150px">GÖNDEREN</td>
          <td>KONU: <?php echo $fk->title; ?><!--  ( Okunma Sayısı: <?php echo $fk->view; ?>) --><div class="pull-right"># <?php echo $a+1; ?></div></td>
        </tr>
        <tr>
          <td>
            <p class="yazar"><?php echo yazar($key->user_id); ?></p>

            <?php if (avatar($key->user_id)){ ?>
             <img src="<?php echo baseUrl(avatar($key->user_id)); ?>" alt="" width="140" height="140">  
            
            <?php }else{ ?>
             <img src="<?php echo baseUrl(); ?>img/user.png" alt="" width="140" height="140">  
            <?php } ?>
            
            
          </td>
          <td>
            <p class="konu_baslik"><?php echo $fk->title; ?> <br>  
              <kp class="konu_tarih">« <?php echo tcevir($key->insertDate,'1'); ?> »</kp>

            </p>
            <p class="konu_mesaj"> <?php echo $key->content; ?></p>
          </td>
        </tr>
        <tr class="forum_title">
          <td colspan="2"  >

          <div class="pull-right" style="color:#fff;">

              <?php 
              
              if(Session::select('userid') == $key->user_id){?>
                <a href="/forum/konu_duzenle/<?php echo $key->id; ?>" class="btn btn-default btn-sm">Düzenle</a> 
                <!-- <a href="/forum/konusil/<?php echo $key->id; ?>" class="btn btn-default btn-sm">Sil</a> -->
                <a href="#" data-id="<?php echo $key->id; ?>"  class="btn btn-default btn-sm cevapsil">Sil</a>
              <?php } ?>
            </div>
           
          </td>
        </tr>
      </table>
     <?php endforeach ?>
      <hr>
      <?php if (Session::select('userid')){ ?>
      
      <form action="" method="post">
      <div class="form-group">
        <label>Cevap Yaz</label>
        <textarea name="konu_cevap" id="editor2" cols="30" rows="5"></textarea>
      </div>
      <div class="form-group">
        <input type="hidden" name="title_seo" value="<?php echo $fk->title_seo; ?>">
        <input type="hidden" name="konu_id" value="<?php echo $fk->id; ?>">
        
        <button class="btn btn-primary btn-block" type="submit"> Cevabı Gönder.</button>
      </div>  
    </form>
      <?php } ?>

		</div>
	</div>
</section>
<!-- stop FORUM BLOG --> 
<?php import::view(TEMA.'footer'); ?>