<?php import::view(TEMA.'header'); ?>
<!-- start nerdeyim bloğu -->
<section id="Page-title" class="Page-title-Style1">
	<div class="container inner-Pages">
   <div class="row">
    <div class="Page-title">
      <div class="col-md-6 Title-Pages">
       <h2>Pisi Linux Blog </h2>
     </div>
     <div class="col-md-6 Catogry-Pages">
       <p>Buradasınız :  <a href="<?php echo baseUrl(); ?>">Anasayfa</a> / Forum </p>
     </div>
   </div>
 </div>
</div>
</section>
<!-- stop nerdeyim bloğu  -->

<!-- start FORUM BLOG -->

<section id="Forum" class="light-wrapper">
	<div class="container inner">

		<div class="row">
			<h2>Pisilinux Forum ( BETA )</h2>	
			<?php foreach (forum_kategori() as $key1): ?>
				<div class="panel panel-info">
			  <div class="panel-heading"><?php echo $key1->adi ?></div>
			
			  	<table class="table td">
					<?php foreach (forum_kategori($key1->id) as $key ): ?>
					<tr>
			  			<td>
			  				<img src="<?php echo baseUrl('upload/pisilinux_pisi.png') ?>" alt="" style="width: 50px;float: left;margin-right: 10px;">
			  				<p class="forum_baslik"><a href="<?php echo baseUrl('forum/konulist/').$key->id.'-'.$key->adi_seo; ?>"><?php echo $key->adi ?></a></p>
			  				<p class="forum_aciklama"><?php echo $key->aciklama ?></p>
			  					
			  			</td>
			  			<td width="150" style="text-align: center; vertical-align: middle;color:#2e2e2e; font-size: 11px;">
			  				
			  				<strong><?php echo iletisayisi($key->id); ?></strong> Konu <br>
			  				<strong>0<?php //echo message_count($key->id);?></strong> İleti<br>


			  			</td>
			  			<td width="300" style=" font-size: 11px;color:#2e2e2e;">
			  				<strong>Son İleti:</strong> 
			  			</td>
			  		</tr>
					<?php endforeach ?>
			  	
			  	</table>
			</div>				
			<?php endforeach ?>
			<!--  
			<div class="panel panel-info">
				<div class="panel-heading">Son İletiler</div>
				
					<table class="table">
						<thead>
							<tr>
								<th>İLETİ</th>
								<th>GÖNDEREN</th>
								<th>FORUM</th>
								<th>TARİH</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach (soniletiler('5') as $ileti): ?>
							<tr>
								<td><a href="<?php echo baseUrl('forum/konu/'.$ileti->content_id.'-'.$ileti->title_seo); ?>"><?php echo $ileti->title ?></a></td>
								<td><?php echo yazar($ileti->user_id); ?></td>
								<td><?php echo forum_kategori_adi($ileti->category_id) ?></td>
								<td><?php echo tcevir($ileti->insertDate,'1'); ?></td>
							</tr>
							
						<?php endforeach ?>
							
						</tbody>
					</table>
				
			</div>
			 -->
		</div>
	</div>
</section>
<!-- stop FORUM BLOG -->
<?php import::view(TEMA.'footer'); ?>