$(function(){

    $("#slider1").responsiveSlides({
        auto: true,           // Boolean: Animate automatically, true/false
        speed: 1000,          // Integer: Speed of the transition, in ms
        timeout: 4000,        // Integer: Time between transitions, in ms
        pager: false,         // Boolean: Show pager, true or false
        nav: false,           // Boolean: Show navigation, true or false
        prevText: "Previous", // String: Text for the "previous" button
        nextText: "Next",     // String: Text for the "next" button
        maxwidth: "none",     // Integer: Max-width of the slideshow, in px
        controls: "",         // Selector: Where controls should be appended
        namespace: "rslides"  // String: change the default namespace used
    });

 $('.cevapsil').click(function(){
     if (confirm('Bunu silmek istediğinizden emin misiniz?')) {
    var silid = $(this).attr("data-id");
    $("[data-id='"+silid+"']").hide('slow');
     $.post("../../ajax/forumcevapsil",{id: silid},
    function(data, status){});
 };
    });
        

    $(".indir").click(function(){
        var  id = $(this).data('id');

        $.post('../../ajax/downup',{ count: id },function(result){
            res = result.split("-");
            $('#'+res['1']).html(res['0']);
            Cookies.set('downup', '1');
        });

    });
   
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var saat = today.getHours()+':'+today.getMinutes()+':'+today.getSeconds();
    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd;
    } 
    if(mm<10){
        mm='0'+mm;
    } 
    var today = dd+'-'+mm+'-'+yyyy+' '+saat;
    document.getElementById("currentDate").value = today;
	//$('#currentDate').val(currentDate); 


    $('ul li:first-child').addClass("active");
    $('div div:first-child').addClass("active in");
    
    $("#uyeformu").on("keyup", function(event){
        form_kontrolu();
    });

    $("#sifrekontrol").on("keyup", function(event){
        sifrekontrolu();
    });




});

//<![CDATA[
  bkLib.onDomLoaded(function() {
       // new nicEditor().panelInstance('area1');
       // new nicEditor({fullPanel : true}).panelInstance('area2');

        new nicEditor({           
            iconsPath : 'https://cdnjs.cloudflare.com/ajax/libs/NicEdit/0.93/nicEditorIcons.gif',
            fullPanel : true,
        }).panelInstance('editor2');
       
  });

  // new nicEditor({buttonList : ['fontSize','bold','italic','underline','strikeThrough','subscript','superscript','html','image']}).panelInstance('area4');
       // new nicEditor({maxHeight : 100}).panelInstance('area5');
  //]]>

var form_kontrolu = function(){
    var kadi = $("#username").val();
    var sifre1 = $("#password").val();
    var sifre2 = $("#password2").val();
    var email = $("#email").val();
    var atpos=email.indexOf("@");
    var dotpos=email.lastIndexOf(".");

    if ( kadi==null || kadi=="" || kadi.length < 3 ){
        $('.uyari').addClass('uyari1');

        $('.uyari').html("Adınız ve soyadınız 3 karakterden az olamaz");
    }else if ( atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length ){
        $('.uyari').html("Lütfen geçerli email adresi girin");
    }else if ( sifre1 == null || sifre1 == "" || sifre2 == null || sifre2 == "" ){
        $('.uyari').html("Lütfen şifreyi boş bırakmayın");        
    }else if ( !(sifre1 == sifre2) ){
        $('.uyari').html("Şifreleriniz eşleşmiyor");  
    }else{
        $('.uyari').removeClass('uyari1');
        $('.uyari').empty();
        $('#uye_formu').removeAttr('onsubmit');
    }
}

var sifrekontrolu = function(){

    var sifre1 = $("#password").val();
    var sifre2 = $("#password2").val();

    if (sifre1 == null || sifre1 == "" || sifre2 == null || sifre2 == "" ){
        $('.uyari').html("Lütfen şifreyi boş bırakmayın");        
    }else if ( !(sifre1 == sifre2) ){
        $('.uyari').html("Şifreleriniz eşleşmiyor");  
    }else{
        $('.uyari').removeClass('uyari1');
        $('.uyari').empty();
        $('#sifrekontrol').removeAttr('onsubmit');
    }
}