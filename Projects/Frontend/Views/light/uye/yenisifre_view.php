<?php import::view(TEMA.'header'); ?>

<section id="Elements-Forms" class="light-wrapper">
	<div class="container inner-Pages">
    	<div class="row">
        	<div class="col-md-12">
                <div class="title-section text-center">
                    <h3>PİSİLİNUX YENİ ŞİFRE</h3>
                    <div class="line-break"></div>
                </div>
            </div>
        </div>
		<div class="divcod30"></div>
    	<div class="row">
        	<div class="col-md-12">
                 <div class="uyari"></div>  
				<form action="" method="post" class="leave-comment contact-form" id="sifrekontrol" autocomplete="off">
					<div class="Contact-form">
                        					
						<div class="form-input">
							<label>Şifreniz</label>
							<input type="password" name="password"  id="password" required>
						</div>

                        <div class="form-input">
                            <label>Şifrenizi Tekrar Yazın</label>
                            <input type="password" name="password2" id="password2"  required>
                        </div>

                        <div class="form-input">
                            <input type="hidden" name="id" value="<?php echo $id; ?>">
                             <input type="hidden" name="act" value="<?php echo $act; ?>">
                            <input type="submit" class="btn btn-block btn-primary " value="ŞİFREMİ DEĞİŞTİR">
                        </div>
                    </form>
                </div>
				
			</div>

       
      
        </div>
    </div>
</section>
<?php import::view(TEMA.'footer'); ?>