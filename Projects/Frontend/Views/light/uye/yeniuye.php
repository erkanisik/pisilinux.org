<?php import::view(TEMA.'header'); ?>

<section id="Elements-Forms" class="light-wrapper">
	<div class="container inner-Pages">
    	<div class="row">
        	<div class="col-md-12">
                <div class="title-section text-center">
                    <h3>PİSİLİNUX YENİ ÜYE KAYIT</h3>
                    <p>Sitemize üye olarak forum, hata kaydı ve bir çok olanaklardan yararlanabilirsiniz. <br>Sizi daha iyi tanımak adına ( Eğitim, Meslek, Beceriler ) alanlarını doldurursanız memnun oluruz. </p>
                    <div class="line-break"></div>
                </div>
            </div>
        </div> 
		<div class="divcod30"></div>
    	<div class="row">
        	<div class="col-md-12">
             <div class="uyari"></div>  
             YILDIZLI ALANLAR MECBURİ ALANLARDIR.
				<form action="" method="post" class="leave-comment contact-form" id="uyeformu" enctype="multipart/form-data">
					<div class="Contact-form">
						<div class="form-input">
							<label>*Adınız Soyadınız</label><br>
							<input type="text" name="username" id="username" required>
						</div>
						
						<div class="form-input">
							<label>*Eposta Adresiniz</label><br>
							<input type="email" name="email" id="email" required>
						</div>

						<div class="form-input">
							<label>*Şifreniz</label>
							<input type="password" name="password" id="password"  required>
						</div>

						<div class="form-input">
							<label>*Şifreniz Tekrar</label>
							<input type="password" name="password2" id="password2"  required>
						</div>

						<div class="form-input">
							<label>Eğitiminiz</label>
							<input type="text" name="egitim">
						</div>


						<div class="form-input">
							<label>Mesleğiniz</label>
							<input type="text" name="meslek">
						</div>

						<div class="form-input">
							<label>Becerileriniz</label>
							<input type="text" name="beceri">
						</div>


						<div class="form-input">
                            <input type="submit" class="btn btn-block btn-primary " value="ÜYE OL">
                        </div>
                        
						
					</div>
				</form>
			</div>

       
      
        </div>
    </div>
</section>
<?php import::view(TEMA.'footer'); ?>