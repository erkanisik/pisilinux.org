<?php import::view(TEMA.'header'); ?>

<section id="Elements-Forms" class="light-wrapper">
	<div class="container inner-Pages">
    	<div class="row">
        	<div class="col-md-12">
                <div class="title-section text-center">
                    <h3>PİSİLİNUX ÜYE GİRİŞİ</h3>
                    <div class="line-break"></div>
                </div>
            </div>
        </div>
		<div class="divcod30"></div>
    	<div class="row">
        	<div class="col-md-12">
				<form action="" method="post" class="leave-comment contact-form" autocomplete="off">
					<div class="Contact-form">
                     
						<div class="form-input">

							<label>Eposta Adresiniz</label><br>
							<input type="email" name="email" value="" >
						</div>
                       
                        <input type="hidden" id="currentDate" name="currentDate" value="">  
                       
						<div class="form-input">
							<label>Şifreniz</label>
							<input type="password" name="pass"  >
						</div>

                        <div class="form-input">
                            
                            <input type="submit" class="btn btn-block btn-primary " value="GİRİŞ YAP">
                        </div>
                    </form>
                </div>
				<div class="col-md-12">
                            
                         <!--   <a href="#"><button class="btn btn-danger">Şifremi Unuttum?</button></a> -->
                             <a href="<?php echo baseUrl('uye/yeniuye'); ?>"><button class="btn btn-info">ÜYE OL</button></a>
                             <a href="<?php echo baseUrl('uye/sifremiunuttum'); ?>"><button class="btn btn-info">ŞİFREMİ UNUTTUM</button></a>
                        </div>
			</div>

       
      
        </div>
    </div>
</section>
<?php import::view(TEMA.'footer'); ?>