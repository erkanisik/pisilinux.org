<?php import::view(TEMA.'header'); ?>
<style>
	strong{
		color:red;
		font-weight: bolder;
	}
</style>
<section id="Contact" class="light-wrapper"> 
	<div class="container inner">
    	<div class="row">
        	<div class="col-md-12">
                <div class="title-section text-center">
                    <h3>PİSİLİNUX İLETİŞİM</h3>
                    <div class="line-break"></div>
                </div>
                <div class="description-section text-center">
                    <p>Pisilinux Hakkında her türlü<strong> öneri, fikir, teklif, yardım ve şikayet</strong>lerinizi yazabilirsiniz...</p>
                    <p><strong>Sizlere dönüş yapabilmemiz için email adresinizin geçerli bir email adresini yazmanız önemlidir</strong> </p>
                </div>
            </div>
        </div>
        <div class="divcod30"></div>
        <div class="row">
        	<form action="" method="post">
			<div class="col-md-12">
				<div class="Contact-Form">
					<form class="leave-comment contact-form" method="post" action="style/php/contact.php" id="cform" autocomplete="on">
						<div class="Contact-us">
							<div class="form-input">
								<input type="text" name="name" placeholder="Adınız Soyadınız" required>
							</div>
							<div class="form-input">
								<input type="email" name="email" placeholder="Email Adresiniz" required>
							</div>
							<div class="form-textarea">
								<textarea class="txt-box textArea" name="message" cols="40" rows="7" id="messageTxt" placeholder="Mesajınız" spellcheck="true" required></textarea>
							</div>
							<div class="form-submit">
								 <input type="hidden" id="currentDate" name="currentDate" value="">  
								<input type="submit" class="btn btn-large main-bg" value="Gönder">
							</div>
						</div>
					</form>
				</div>
			</div>
			</form>
        </div>
    </div>
</section>
<?php import::view(TEMA.'footer'); ?>