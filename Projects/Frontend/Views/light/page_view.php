<?php include'header.php';  ?>
<section id="Latest-News" class="light-wrapper">
	<div class="container inner">
        <div class="row">
        	<div class="col-md-12 Blog-Full">
				<div class="Title-Single">
                	<div class="Top-Title-Blog">
                    	<h2><?php echo $content->icerik_baslik; ?></h2>
                    </div>
                   <hr>
                </div>
                <div class="Single-Blog-Content">
                	<div class="Post-Images">
                    	<img src="<?php echo baseUrl($content->icerik_resim); ?>" alt="<?php echo $content->icerik_baslik; ?>"  style="width: 500px;">
                    </div>
                    <div class="Post-Content">
                    	<p>
                    		<?php  echo $content->icerik_detay ?>
                    	</p>
                    </div>
                </div>

                <div class="Under-Post">
                    <div class="tags">
                        <i class="fa fa-tags"></i><?php echo etiket($content->icerik_tag); ?>
                    </div>
					<div id="shareIconsCount" class="Social-Content Social-Blog jssocials">
						<div class="jssocials-shares">
							<div class="jssocials-share jssocials-share-facebook">
								<a target="_blank" href="https://facebook.com/sharer/sharer.php?u=<?php echo baseUrl('content/'.$content->icerik_id.'-'.$content->baslik_seo.'.html'); ?>" class="jssocials-share-link">
									<i class="fa fa-facebook jssocials-share-logo"></i>
								</a>
								<div class="jssocials-share-count-box jssocials-share-no-count">
									<span class="jssocials-share-count"></span>
								</div>
							</div>
							<div class="jssocials-share jssocials-share-twitter">
								<a target="_blank" href="https://twitter.com/share?url=<?php echo baseUrl('content/'.$content->icerik_id.'-'.$content->baslik_seo.'.html'); ?>" class="jssocials-share-link">
									<i class="fa fa-twitter jssocials-share-logo"></i>
								</a><div class="jssocials-share-count-box jssocials-share-no-count"><span class="jssocials-share-count"></span></div></div>

                                <div class="jssocials-share jssocials-share-googleplus"><a target="_blank" href="https://plus.google.com/share?url=<?php echo baseUrl('content/'.$content->icerik_id.'-'.$content->baslik_seo.'.html'); ?>" class="jssocials-share-link"><i class="fa fa-google-plus jssocials-share-logo"></i></a><div class="jssocials-share-count-box jssocials-share-no-count"><span class="jssocials-share-count"></span></div></div>

                                <div class="jssocials-share jssocials-share-linkedin"><a target="_blank" href="https://www.linkedin.com/shareArticle?url=<?php echo baseUrl('content/'.$content->icerik_id.'-'.$content->baslik_seo.'.html'); ?>" class="jssocials-share-link"><i class="fa fa-linkedin jssocials-share-logo"></i></a><div class="jssocials-share-count-box jssocials-share-no-count"><span class="jssocials-share-count"></span></div></div>

                                <div class="jssocials-share jssocials-share-pinterest">
                                    <a target="_blank" href="https://pinterest.com/pin/create/bookmarklet/?&amp;url=<?php echo baseUrl('content/'.$content->icerik_id.'-'.$content->baslik_seo.'.html'); ?>&amp;description=<?php echo Myfunc::kelimebol($content->icerik_detay,100); ?>" class="jssocials-share-link"><i class="fa fa-pinterest jssocials-share-logo"></i></a><div class="jssocials-share-count-box jssocials-share-no-count"><span class="jssocials-share-count"></span></div></div></div>

                        </div>
                </div>
				<div class="divcod50"></div>
<!--
                <div class="Single-Blog-Comment">
                	<div class="Title-Comment">
                    	<h3>3 Comments</h3>
                    </div>
                    <div class="line-break"></div>
                    <div class="divcod20"></div>
                    <div class="Comments-Post">
                    	<ul>
                        	<li>
                                <div class="Block-Comment">
                                    <img src="style/images/blog/Img-Comment.png" alt="post footer" width="70" height="70">
                                    <h4>Habaza</h4>
                                    <span>Oct 14, 2014 - 08:07 pm <a href="#">Reply</a></span>
                                    <p>Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla mus, dignissim neque, nulla neque. </p>
                                </div>
                                <ul>
                                    <li>
                                        <div class="Block-Comment">
                                            <img src="style/images/blog/Img-Comment.png" alt="post footer" width="70" height="70">
                                            <h4>Habaza</h4>
                                            <span>Oct 14, 2014 - 08:07 pm <a href="#">Reply</a></span>
                                            <p>Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla mus, dignissim neque, nulla neque. </p>
                                        </div>
                                    </li>
                                </ul>
                        	</li>
                            <li>
                                <div class="Block-Comment">
                                    <img src="style/images/blog/Img-Comment.png" alt="post footer" width="70" height="70">
                                    <h4>Habaza</h4>
                                    <span>Oct 14, 2014 - 08:07 pm <a href="#">Reply</a></span>
                                    <p>Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla mus, dignissim neque, nulla neque. </p>
                                </div>
                            </li>
                            <li>
                                <div class="Block-Comment">
                                    <img src="style/images/blog/Img-Comment.png" alt="post footer" width="70" height="70">
                                    <h4>Habaza</h4>
                                    <span>Oct 14, 2014 - 08:07 pm <a href="#">Reply</a></span>
                                    <p>Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla mus, dignissim neque, nulla neque. </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="Add-Comment">
                    <div class="Title-Comment">
                    	<h3>Add Comment</h3>
                    </div>
                    <div class="line-break"></div>
                    <div class="divcod30"></div>
                    <div class="Comment-Form">
                        <form action="#" class="leave-comment contact-form">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-input">
                                        <input type="text" placeholder="Your Name" required="">
                                    </div>
                                    <div class="form-input">
                                        <input type="email" placeholder="Email" required="">
                                    </div>
                                    <div class="form-input">
                                        <input type="submit" class="btn btn-large main-bg" value="Add Comment">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-input">
                                        <textarea class="txt-box textArea" name="message" cols="40" rows="7" id="messageTxt" placeholder="Comment" spellcheck="true" required=""></textarea>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
!-->
            </div>
        </div>
    </div>
</section>
<?php include'footer.php';  ?>