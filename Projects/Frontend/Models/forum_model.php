<?php 

class Forum_model extends Model{

	
	
	function kategori($par = '0'){
		return DB::where('kat_ustid',$par)->get('forum_kat')->result();
	}

	function konu_kaydet($post){
		$exp = explode('-', $post['konuid']);
		
		DB::insert('forum',[
			'category_id' 		=> $exp['0'], 
			'title' 		=> $post['konubaslik'], 
			'title_seo'	=> seo($post['konubaslik']), 
			'content' 			=> $post['konuicerik'],
			'user_id'		=> Session::select('userid'),
		]);

		if (DB::affectedRows()) {
		redirect(baseUrl('forum/konulist/'.$post['konuid']));
		}
		
	}

	function forumkonu($post){
		$title_seo = explode('.',$post);
		return DB::where('id',$post)->get('forum')->row();
	}

	function cevap($post){
		// $baslik = DB::select('title_seo')->where('title_seo',$post['konu_id'])->get('forum')->value();
			
		DB::insert('forum',[
			'content' 		=> $post['konu_cevap'],
			'content_id'	=> $post['konu_id'],
			'user_id'		=> Session::select('userid'),
		]);

		if (DB::affectedRows()) {

		redirect(baseUrl('forum/konu/'.$post['konu_id'].'-'.$post['title_seo'].'.html'));
		}
	}

	function edit($data){
		return DB::where('id',$data)->get('forum')->row();
	}


	function update($post){
		DB::where('id',$post['content_id'])->update('forum',[
			'title' 		=> $post['konubaslik'], 
			'title_seo'	=> seo($post['konubaslik']), 
			'content' 			=> $post['konuicerik'],
			'user_id'		=> Session::select('userid'),
		]);

		if (DB::affectedRows()) {
		redirect(baseUrl('forum/konu/'.$post['konuid'].'-'.seo($post['konubaslik'])));
		}
		
	}


}