<?php 
/**
* Created By  : Erkan IŞIK
* Created Date: 2017-12-06
* Update Date : 2018-02-18
* 
*/
class Uye extends Controller{
	
	function main(){
		if(method::post()) {$this->user_model->logincheck(method::post());} 
		import::view(TEMA.'uye/index');
	}

	function yeniuye(){
	
		
			if (method::post()){$this->user_model->yeniuye(method::post());}
			import::view(TEMA.'uye/yeniuye');
		
	} 

	function aktivizasyon($act){
		DB::where('aktivizasyon',$act)->update('user',[
			'aktivizasyon' => '',
			'status' => '1',
		]);
		if (DB::affectedRows()) {

			import::view(TEMA.'uye/basarili');
		}
	}

	function sifremiunuttum(){
		if (method::post()) {$this->user_model->sifremiunuttum_model(method::post());}
		import::view(TEMA.'uye/sifremiunuttum_view');
	}

	function ref($data){
	
		$exp = explode('-', $data);
		$ref = $exp['0'].'/'.$exp['1'].'/'.$exp['2'].'-'.seo($exp['3']);
		Session::insert('ref', $ref);
			
		if(method::post()) {$this->user_model->logincheck(method::post());} 
		import::view(TEMA.'uye/index');
			
	}

	function yenisifre($act){

		if (method::post()) {$this->user_model->yenisifre(method::post());}
		
			$data['id'] = DB::select('user_id')->where('aktivizasyon',$act)->get('user')->value();
			$data['act'] = $act;
			import::view(TEMA.'uye/yenisifre_view',$data);
		
	}

	 function logout(){
        Session::deleteAll();
        redirect(baseUrl());
    }
}