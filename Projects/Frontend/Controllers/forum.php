<?php 

/*
* Created By: Erkan IŞIK
* Created Date: 2017-12-05
* Update Date: 2018-01-22
*/
class Forum extends Controller{

	function main()
	{
				//$data['forum_list'] = $this->forum_model->kategori();
		//import::view(TEMA.'forum/index');
		define('TITLE', ' Forum');
		import::view(TEMA.'forum/index1');
	}

	function forum1(){
		import::view(TEMA.'forum/index1');
	}

	function konulist($post){

		$par = explode('-',$post);

		$data['konular'] = DB::where('category_id',$par['0'])->get('forum')->result();

		$data['kategori'] = $par['0'];
		define('TITLE', ' '.forum_kategori_adi($data['kategori']));
		import::view(TEMA.'forum/konulist',$data);
	}


	function yenikonu($id){
		
		$data['id'] = $id;
		
		if (method::post()) {$this->forum_model->konu_kaydet(method::post());}
		import::view(TEMA.'forum/yenikonu',$data);
	}

	function konu($id){
		$par = explode('-',$id);
		if (method::post()) {$this->forum_model->cevap(method::post());}

		$data['fk'] = $this->forum_model->forumkonu($par['0']);
		define('TITLE', ' '.$data['fk']->title_seo);

		import::view(TEMA.'forum/konu',$data);
	}

	function konu_duzenle($id){
		if (method::post()) {$this->forum_model->Update(method::post());}
		$data['dzn'] = $this->forum_model->edit($id);

		import::view(TEMA.'forum/konu_duzenle',$data);
	}

	function cevapsil($id){
		
	}

}		