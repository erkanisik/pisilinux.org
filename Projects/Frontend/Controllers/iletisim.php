<?php
/*
* Created By: Erkan IŞIK
* Created Date: 2017-05-09
* Update Date: 2018-02-19
*/

class Iletisim extends Controller{


    function main(){
        
            $mesaj = '
            Mesajı Gönderen: '.method::post('name').'<br>
            Mail Adresi: '.method::post('email').'<br>
            Mesaj: '.method::post('message');
       

        if(method::post()){

            Email::sender('form@pisilinux.org', 'Pisilinux İletişim')
            ->receiver('info@pisilinux.org')
            ->subject(method::post('name').' mesajı var')
            ->message($mesaj)
            ->send();

            if (!Email::error()) {
DB::insert('iletisim',[
    'isim'  => method::post('name'),
    'email'  => method::post('email'),
    'mesaj' => method::post('message'),
    'date'  => method::post('currentDate'),
]);

                jalert('Mesajınız başarıyla gönderildi.\nEn kısa sürede sizinle iletişime geçilecektir.'); 
                git();
            }
        }

    	import::view(TEMA.'iletisim');
   
    }
} // class sonu
