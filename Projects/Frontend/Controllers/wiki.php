<?php
/*
* Created By  : Erkan IŞIK
* Created Date: 2017-12-24
* Update Date : 2018-02-15
*/
class Wiki extends Controller{


    public function main(){
		$data['catlist'] = DB::where('kat_ustid',30)->get('wiki_kat')->result();
		import::view(TEMA.'wiki/index',$data);

    }


		function cat($kat_id){
			$id = explode('-', $kat_id);
			$data['wikikat'] = DB::where('katid',$id['0'])->get('wiki')->result();
		
			import::view(TEMA.'wiki/wikilist_view',$data);
		}

	function icerik($p = ''){
		$p = explode('-', $p);
		$data['icerik'] = DB::where('id',$p[0])->get('wiki')->row();
		import::view(TEMA.'wiki/content_view',$data);
	} 


   
}
?>
