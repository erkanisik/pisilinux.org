<?php 

/*
* Created By: Erkan IŞIK
* Created Date: 2017-12-07
* Update Date: 2017-12-07
*/
	class Indir extends Controller{
		
		function main(){

			$data['liste'] = DB::orderBy('id','desc')->get('download')->result();

			import::view(TEMA.'indir/index',$data);

			}
	}