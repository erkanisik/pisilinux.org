<?php 
/**
* Created By  : Erkan IŞIK
* Created Date: 2017-12-24
* Update Date : 2017-12-24
* 
*/
class Page extends Controller{
	
	function main($p = ''){
		$p = explode('-', $p);
		$data['content'] = DB::where('icerik_id',$p[0])->get('content')->row();
		import::view(TEMA.'page_view',$data);
	}
}