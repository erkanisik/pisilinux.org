<?php
/*
* Created By: Erkan IŞIK
* Created Date: 2017-05-09
* Update Date: 2018-04-26
*/
class Home extends Controller{

  	function main(){

    	$data['sldr'] = DB::where('durum','1')->orderBy('sira','asc')->get('slider')->result();
    	$data['content'] = DB::orderBy('icerik_id','desc')->where('icerik_tur','icerik')->limit('3')->get('content')->result();
    	Import::view(TEMA.'index',$data);


	}  
}
