<?php 

/*
* Created By  : Erkan IŞIK
* Created Date: 2017-12-05
* Update Date : 2018-02-14
*/
	class Blog extends Controller{
		
		function main(){
			define('TITLE', ' Blog');
			$data['content'] =  $this->blog_model->content_list();
			import::view(TEMA.'blog/index1',$data);
		}


		function cat($kat_id){
			$id = explode('-', $kat_id);
			$seo = explode('.', $id['1']);
			$data['catlist'] = DB::where('icerik_katid',$id['0'])->get('content')->result();
		    define('TITLE', ' '.$seo['0']);
			import::view(TEMA.'blog/category_view',$data);
		}

		function newslist($par){
			$id = explode('-', $par);
			$data['newslist'] = DB::where('icerik_katid',$id['0'])->get('content')->result();
		
			import::view(TEMA.'blog/newslist_view',$data);

		}


	function icerik($p = ''){
		$p = explode('-', $p);


		$data['icerik'] = DB::where('icerik_id',$p[0])->get('content')->row();
		
		define('TITLE', ' '.$data['icerik']->baslik_seo);
		import::view(TEMA.'blog/content_view',$data);
	} 

		
	}